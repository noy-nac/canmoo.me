

from car import Car
from frog import Frog

from random import randint

cars = []
for i in range(-200, 200 + 1, 40):
    cars.append(Car(randint(-400, 400), i))

player = Frog(0, -300)

while True:
    player.draw(10)
    for c in cars:
        c.draw(15)
        c.move()
        c.check_collide(player, 24)


player.screen.mainloop()
