#############
# player.py #
#############

class Player:

    def __init__(self, irow, icol):
        self.row = irow
        self.col = icol

    def move(self, drow, dcol):
        self.row += drow
        self.col += dcol

###########
# zork.py #
###########

overworld_map = [
    ["Taos",   "Guadalupe - N",  "UT Dorms",   "Turtle Pond"  ],
    ["Shops",  "Guadalupe",      "West Mall",  "Tower",       "East Mall"],
    ["",       "Guadalupe - S",  "Dobie",      "Fountain"     ],
]

item_map = [
    [["sword"], ["snowcone"], [""], ["turtle"] ],
    [["books"], [], [], [], []],
    [[], [], [], ["key"]],
]

inv = []

def pickup(row, col, item):

    for i in item_map[row][col]:
        if i == item:
            item_map[row][col].remove(item)
            inv.append(item)
    
    # ALTERNATE
    if item in item_map[row][col]:
        item_map[row][col].remove(item)
        inv.append(item)

player = Player(1, 3)

alive = True
while alive:
    # 1. describe world
    print(overworld_map[player.row][player.col], end="\n")
    print("Items", item_map[player.row][player.col], end="\n")
    # 2. what to do?
    command = input(">")

    parts = command.split(" ")
    #print(parts)

    if parts[0] == "go":
        if parts[1] == "north":
            player.move(-1, 0)
        
    if parts[0] == "take":
        pickup(player.row, player.col, parts[1])
    
    if parts[0] in ["inventory", "inv"]:
        print("In your inventory", inv)

