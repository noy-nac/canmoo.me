
import java.lang.*;
import java.util.*;

public class UIL {

    public static String inBin(int i) {
        return String.format("%32s", Integer.toString(i, 2)).replace(" ","0");
    }

    public static void bit() {
        int i = -100;
        
        int j = i << 1;

        int k = i >>> 1;
        k = k << 1;

        System.out.println(inBin(i));
        System.out.println(inBin(j));
        System.out.println(inBin(k));
    }

    public static void scan() {

    }

    public static void map() {

    }

    public static void main(String[] args) {
        bit();
    }
}


