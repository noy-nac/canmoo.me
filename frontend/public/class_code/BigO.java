
import java.util.*;

public class BigO {

    public static int linearSearch(int[] array, int target, int[] ctr) {
        for(int i = 0; i < array.length; i++) {
            ctr[0]++;
            if(array[i] == target) {
                return i;
            }
        }
        return -1;
    }

    public static int binarySearch(int[] array, int target, int[] ctr) {
        //System.out.println("target: " + target);
        int low = 0, high = array.length - 1;
        int mid = (low + high) / 2;

        while(low <= high) {
            ctr[0]++;
            if(array[mid] == target) {
                return mid;
            }
            else if(array[mid] < target) {
                low = mid + 1;
            }
            else { // array[mid] > target
                high = mid - 1;
            }
            mid = (low + high) / 2;
        }
        //System.out.println("LOW: " + low + " val: " + array[low]);
        //System.out.println("MID: " + mid  + " val: " + array[mid]);
        //System.out.println("HI : " + high + " val: " + array[high]);
        return -1;
    }

    public static void main(String[] args) {
        int[] array10     = new int[10];
        int[] array100    = new int[100];
        int[] array1000   = new int[1000];
        int[] array5000   = new int[5000];
        int[] array10000  = new int[10000];
        int[] array15000  = new int[15000];
        int[] array20000  = new int[20000];
        int[] array25000  = new int[25000];
        int[] array100000 = new int[100000];

        int[][] arrarr = {
            array10, array100, array1000, array10000, array100000 
        };

        Random rand = new Random();

        for(int[] arr : arrarr) {
            // each array starts with a random value
            arr[0] = rand.nextInt(10);

            for(int i = 1; i < arr.length; i++) {
                // each subsequent element is greater than or equal to the previous
                arr[i] = arr[i-1] + rand.nextInt(10);
            }
        }

        //System.out.println("Array  10: " + Arrays.toString(array10));
        //System.out.println("Array 100: " + Arrays.toString(array100));

        System.out.println("LINEAR SEARCH");
        for(int[] arr : arrarr) {
            int[] ctr = {0};

            // search each array 100 times for a random target
            for(int i = 0; i < 100; i++) {
                int tgt = rand.nextInt(arr[arr.length-1]+1);
                linearSearch(arr, tgt, ctr);
            }
            ctr[0] /= 100;

            System.out.println("Length: " + arr.length + ", Iterations: " + ctr[0] + ", Ratio: " + ((double)ctr[0] / arr.length));
        }

        System.out.println("\nBINARY SEARCH");
        for(int[] arr : arrarr) {
            int[] ctr = {0};

            // search each array 100 times for a random target
            for(int i = 0; i < 100; i++) {
                int tgt = rand.nextInt(arr[arr.length-1]+1);
                binarySearch(arr, tgt, ctr);
            }
            ctr[0] /= 10;

            System.out.println("Length: " + arr.length + ", Iterations: " + ctr[0] + ", Ratio: " + ((double)ctr[0] / arr.length));
        }


    }

}