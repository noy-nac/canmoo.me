import random

from turtle import *

class Snail(Turtle):
    def __init__(self, x, y, lkey, rkey, movekey):
        super().__init__()
        self.speed(10000)
        self.penup()
        self.goto(x,y)
        self.pendown()
        self.pencolor("purple")
        self.screen.onkeypress(self.turn_left, lkey)
        self.screen.onkeypress(self.turn_right, rkey)
        self.screen.onkeypress(self.go_forward, movekey)
        self.screen.listen()

    def turn_left(self):
        self.left(10)

    def turn_right(self):
        self.right(10)
    
    def go_forward(self):
        self.forward(10)

    """
    def accelerate(self):
        self.forward(10)

    def go_right(self):
        dir = self.heading()
        dir = (360 + dir) % 360
        h = dir
        if dir > 0 and dir < 180:
            h -= 15
        else:
            h += 15

        if (dir - 10 < 0 and dir + 10 > 0) or (dir - 10 < 360 and dir + 10 > 360):
            h = 0
        
        self.setheading(h)
        self.accelerate()

    def go_left(self):
        dir = self.heading()
        dir = (360 + dir) % 360
        h = dir
        if dir > 0 and dir < 180:
            h += 15
        else:
            h -= 15

        if dir - 10 < 180 and dir + 10 > 180:
            h = 180
        
        self.setheading(h)
        self.accelerate()

    def go_up(self):
        dir = self.heading()
        dir = (360 + dir) % 360
        h = dir
        if dir > 90 and dir < 270:
            h -= 15
        else:
            h += 15

        if dir - 10 < 90 and dir + 10 > 90:
            h = 90
        
        self.setheading(h)
        self.accelerate()
    
    def go_down(self):
        dir = self.heading()
        dir = (360 + dir) % 360
        h = dir
        if dir > 90 and dir < 270:
            h += 15
        else:
            h -= 15

        if dir - 10 < 270 and dir + 10 > 270:
            h = 270
        
        self.setheading(h)
        self.accelerate()
    """
