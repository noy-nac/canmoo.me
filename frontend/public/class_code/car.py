from turtle import *

from random import uniform

class Car(Turtle):
    def __init__(self, x, y):
        super().__init__()
        self.speed(0)
        self.hideturtle()
        self.screen.tracer(0)
        self.penup()
        self.color(uniform(0.4, 0.9), uniform(0.4, 0.9), uniform(0.4, 0.9))
        self.mph = 15 * uniform(0.2, 0.8)
        self.goto(x,y)
        self.setheading(0)
        self.stopped = False

    def draw(self, r):
        self.clear()
        self.sety(self.ycor()-r)
        self.pendown()
        self.begin_fill()
        self.circle(r)
        self.end_fill()
        self.penup()
        self.sety(self.ycor()+r)
        self.screen.update()

    def move(self):
        if self.stopped:
            return

        self.forward(self.mph)

        if self.xcor() > 400:
            self.setx(-400)

    def within_dist(self, other, dist):
        return (other.xcor() - self.xcor()) ** 2 + (other.ycor() - self.ycor()) ** 2 < dist ** 2
    
    def check_collide(self, other, dist):
        if self.within_dist(other, dist):
            self.stopped = True
        else:
            self.stopped = False

            
