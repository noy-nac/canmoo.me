
from turtle import Turtle

class Frog(Turtle):
    def __init__(self, x, y):
        super().__init__()
        self.speed(0)
        #self.hideturtle()
        self.penup()
        self.goto(x, y)
        self.setheading(90)
        self.screen.onkeypress(self.move, 'space')
        self.screen.listen()
    
    def draw(self, r):
        self.clear()
        self.setx(self.xcor()+r)
        self.pendown()
        self.begin_fill()
        self.circle(r)
        self.end_fill()
        self.penup()
        self.setx(self.xcor()-r)
        self.screen.update()
    
    def move(self):
        self.forward(20)

