
// Servo.h is another code file which contains a software abstraction of the servo
// "#include <Servo.h>" tells the macro preprocessor to copy-paste all the code in Servo.h into our program
#include <Servo.h>

const int SERVO_PIN = 11;

// The software abstraction of the servo - has data type "Servo" and the identifier "myServo"
// The software abstraction handles all the digitalReads and digitalWrites necessary to control the servo
// Instead of reading and writing to SERVO_PIN directly, we use an 'interface' provided by the software abstraction
// An abstraction hides the underlying 'implementation details' to simpifly our interaction with the servo
Servo myServo;

void setup() {
  // Instead of pinMode(SERVO_PIN), we type "myServo.attach(SERVO_PIN)"
  // "myServo" followed by the dot operator "." lets us use the abstractions defined in myServo (e.g., "attach", "write")
  // Conceptually, the "attach" command connects the physical servo component to the software abstraction called "myServo"
  // Under the hood, "attach" is likely using commands like pinMode, digitalRead, digitalWrite, etc.
  // HOWEVER, the exact inner-workins are intentionally obscured (abstracted away) to provide us with a HIGH-LEVEL way to use the servo
  myServo.attach(SERVO_PIN);
}

void loop() {

  // Does this code work?
  // What do you expect? And what actually happens? Why????

  /*
  myServo.write(0);
  delay(25);
  myServo.write(180);
  delay(25);
  */

  ///////////////////////////////////////////////////////////////

  // Declare a variable with data type "int" and identifier "deg"
  int deg;

  // Syntax: for( <BEFORE_LOOP_STARTS>; <CONDITION>; <AFTER_EACH_ITERATION> ) { <THIS_CODE_RUNS_EACH_ITERATION> }
  // When the program first encounters a for loop, it runs the code <BEFORE_LOOP_STARTS>
  // Then it evaluates <CONDITION>
  // If <CONDITION> is true, the program enters the loop (otherwise, the program skips the loop entirely)
  // Each time the loop runs, the program runs <THIS_CODE_RUNS_EACH_ITERATION> inside the for loop's "{ }"
  // Next, the program runs <AFTER_EACH_ITERATION>
  // Then, the loop evaluates <CONDITION> again
  // If <CONDITION> is still true, the loop continues (otherwise, the loop ends and and the program moves on)

  // Moves the servo from 0 degrees to 180 degrees
  // "deg = deg + 1" updates the value in deg each iteration by using the previous value of deg and adding 1
  for(deg = 0; deg <= 180; deg = deg + 1) {
    // the "write(POSITION)" abstraction tells the servo to move to a certain POSITION in degrees (0-180)
    myServo.write(deg);
    // "delay(15)" gives the servo component time to carry out the request
    delay(15);
  }

  // Moves the servo back from 180 degress to 0 degrees
  for(deg = 180; deg >= 0; deg = deg - 1) {
    myServo.write(deg);
    delay(15);
  }

  ///////////////////////////////////////////////////////////////

  // How can you make the servo move faster or slower?
  // How does changing the delay time or changing the update to deg change the speed that the servo component moves? Why?

  ///////////////////////////////////////////////////////////////

  // A for loop using only if statements, labels, and goto (look up what labels and gotos are):

  // <BEFORE_LOOP_STARTS>
  // LOOP:
  // if( ! <CONDITION> ) { goto END; }
  // <THIS_CODE_RUNS_EACH_ITERATION>
  // <AFTER_EACH_ITERATION> 
  // goto LOOP;
  // END:

}
