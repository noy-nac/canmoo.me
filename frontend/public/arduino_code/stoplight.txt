// Global variables - we can use them in BOTH void setup() and void loop()

// "const int" tells us some high level info about the data we will describe - called the "data type"
// "const" meaning the value is constant (does not change after assignment)
// "int" meaning the value is an integer (..., -1, 0, 1, 2, 3, ...)

// "RED_PIN", "YELLOW_PIN", "GREEN_PIN" are identifiers - they hold the value (9, 10, 11) that follows "="
// "=" is the assignment operator - e.g., "RED_PIN = 9" means we assign the value "9" to the identifer "RED_PIN"
// ";" tells the compiler to move on to the next statement

const int RED_PIN = 9;
const int YELLOW_PIN = 10;
const int GREEN_PIN = 11;

// Arduino microcrontroll runs this code FIRST
void setup() {
  pinMode(RED_PIN, OUTPUT);
  pinMode(YELLOW_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
}

// After running the code inside "void setup()"'s { },
// Arduino microcontroller runs the code inside "void loop()"'s { } following  again and again forever
// Code inside "{ }" means that code 'belongs' to the function (e.g., "void loop()") or statement preceeding the "{ }"
void loop() {
  
  // Go -> GREEN_LED is on (HIGH); YELLOW_LED and RED_LED are off (LOW)
  digitalWrite(GREEN_PIN, HIGH);
  digitalWrite(YELLOW_PIN, LOW);
  digitalWrite(RED_PIN, LOW);
  delay(1000);

  // Slow -> YELLOW_LED is on (HIGH); GREEN_LED and RED_LED are off (LOW)
  digitalWrite(GREEN_PIN, LOW);
  digitalWrite(YELLOW_PIN, HIGH);
  digitalWrite(RED_PIN, LOW);
  delay(500);

  // Stop -> RED_LED is on (HIGH); GREEN_LED and YELLOW_LED are off (LOW)
  digitalWrite(GREEN_PIN, LOW);
  digitalWrite(YELLOW_PIN, LOW);
  digitalWrite(RED_PIN, HIGH);
  delay(1000);
}
