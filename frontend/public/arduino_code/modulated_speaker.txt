
// The first analog pin is actually pin 14 on the Arduino
// "A0" is replaced by "14" by the compiler
const int POTEN_PIN = A0;
// Similarly, "A1" is actually pin 15 on the Arduino
const int SPEAKER_PIN = A1;

const int BUTTON_PIN = 13;

void setup() {
    pinMode(SPEAKER_PIN, OUTPUT);

    pinMode(POTEN_PIN, INPUT);
    pinMode(BUTTON_PIN, INPUT);

    // 9600 is the baud rate, which tell us the speed in bits per second at which the Arduino communicates back to the laptop
    Serial.begin(9600);
}

void loop() {

    // Turn on the speaker ONLY IF we push the button
    if (digitalRead(BUTTON_PIN) == HIGH) {
        int knobPos = analogRead(POTEN_PIN);

        Serial.print("Knob position (0-1023): ");
        Serial.println(knobPos);

        // Note for analogRead and analogWrite
        // Analog in has 1024 possible values (0-1023)
        // Analog out has 256 possible values (0-255)
        // To match the range of analog in to analog out, we would need to scale by a factor of 1/4 because 1024/4 = 256

        // analogWrite(PIN, VALUE) sends PULSED square waves at a constant frequency (480 Hz)
        // The 256 possible values of analog out determine the pulse density

        // Since the speaker tone is a frequence, we are NOT constrained by the possible values of analog out
        // Play around with the scale factor to create differnt frequencies
        int speakerTone = 2 * knobPos;

        Serial.print("Speaker tone (Hz): ");
        Serial.println(speakerTone);

        // tone(PIN, FREQUENCY, DURATION) sends a CONTINUOUS square wave at a constant frequency for the duration
        tone(SPEAKER_PIN, speakerTone, 200);
    }
}


