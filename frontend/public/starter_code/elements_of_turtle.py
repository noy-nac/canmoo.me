from turtle import Turtle

def my_function():
    bob = Turtle()
    bob.shape("turtle")
    bob.color("red")
    bob.fillcolor("blue")
    bob.begin_fill()
    bob.forward(50)
    bob.right(90)
    bob.forward(50)
    bob.right(90)
    bob.forward(50)
    bob.right(90)
    bob.forward(50)
    bob.end_fill()
    bob.screen.mainloop()

if __name__ == "__main__":
    my_function()