from turtle import Turtle

def my_function():
	bob = Turtle()
	bob.forward(150)
	bob.left(90)
	bob.forward(50)
	bob.left(90)
	bob.forward(50)
	bob.right(90)
	bob.forward(50)
	bob.right(90)
	bob.forward(50)
	bob.left(90)
	bob.forward(50)
	bob.left(90)
	bob.forward(150)
	bob.screen.mainloop()

if __name__ == "__main__":
	my_function()
