
from turtle import Turtle, exitonclick

def draw_n():
    n = Turtle()
    n.color("black")
    n.fillcolor("gold")

    n.pensize(5)

    n.penup()
    n.goto(-210, 0)
    n.pendown()

    n.begin_fill()

    n.setheading(90)
    n.forward(80)

    n.right(90)
    n.forward(20)

    n.goto(n.xcor() + 20, n.ycor() - 50)

    n.setheading(90)
    n.forward(50)
    
    n.right(90)
    n.forward(20)

    n.right(90)
    n.forward(80)

    n.right(90)
    n.forward(20)

    n.goto(n.xcor() - 20, n.ycor() + 50)

    n.setheading(270)
    n.forward(50)

    n.right(90)
    n.forward(20)

    n.end_fill()
    #n.hideturtle()

def draw_a1():
    a1 = Turtle()
    a1.color("black")
    a1.fillcolor("yellow")

    a1.pensize(5)

    a1.penup()
    a1.goto(-150, 0)
    a1.pendown()

    a1.begin_fill()

    a1.goto(a1.xcor() + 20, a1.ycor() + 80)
    
    a1.goto(a1.xcor() + 20, a1.ycor())
    
    a1.goto(a1.xcor() + 20, a1.ycor() - 80)

    a1.goto(a1.xcor() - 20, a1.ycor())

    a1.goto(a1.xcor() - 10, a1.ycor() + 60)

    a1.goto(a1.xcor() - 10*(3/6), a1.ycor() - 20)

    a1.goto(a1.xcor() + 10, a1.ycor())

    a1.goto(a1.xcor() - 10 - 10*(2/6), a1.ycor() - 10)

    a1.goto(a1.xcor() - 10*(1/6), a1.ycor() - 30)

    a1.goto(a1.xcor() - 20, a1.ycor())

    a1.end_fill()
    #a1.hideturtle()

def draw_v():
    v = Turtle()
    v.color("black")
    v.fillcolor("gold")

    v.pensize(5)

    v.penup()
    v.goto(-80, 0)
    v.pendown()

    v.begin_fill()

    v.goto(v.xcor() - 10, v.ycor() + 80)

    v.setheading(0)
    v.forward(20)

    v.goto(v.xcor() + 10, v.ycor() - 80)

    v.goto(v.xcor() + 20, v.ycor() + 80)

    v.setheading(0)
    v.forward(10)

    v.goto(v.xcor() - 20, v.ycor() - 80)

    v.goto(v.xcor() - 30, v.ycor())

    v.end_fill()
    #v.hideturtle()

def draw_a2():
    a2 = Turtle()
    a2.color("black")
    a2.fillcolor("yellow")

    a2.pensize(5)

    a2.penup()
    a2.goto(-30, 0)
    a2.pendown()

    a2.begin_fill()

    a2.goto(a2.xcor() + 20, a2.ycor() + 80)
    
    a2.goto(a2.xcor() + 20, a2.ycor())
    
    a2.goto(a2.xcor() + 20, a2.ycor() - 80)

    a2.goto(a2.xcor() - 20, a2.ycor())

    a2.goto(a2.xcor() - 10, a2.ycor() + 60)

    a2.goto(a2.xcor() - 10*(3/6), a2.ycor() - 20)

    a2.goto(a2.xcor() + 10, a2.ycor())

    a2.goto(a2.xcor() - 10 - 10*(2/6), a2.ycor() - 10)

    a2.goto(a2.xcor() - 10*(1/6), a2.ycor() - 30)

    a2.goto(a2.xcor() - 20, a2.ycor())

    a2.end_fill()
    #a2.hideturtle()

def draw_r1():
    r1 = Turtle()
    r1.color("black")
    r1.fillcolor("gold")

    r1.pensize(5)

    r1.penup()
    r1.goto(30, 0)
    r1.pendown()

    r1.begin_fill()

    r1.setheading(90)
    r1.forward(80)

    r1.right(90)
    r1.forward(40)

    r1.penup()
    r1.goto(r1.xcor(), r1.ycor() - 40)
    r1.pendown()
    r1.circle(20, 180)
    r1.penup()
    r1.goto(r1.xcor(), r1.ycor() - 40)
    r1.pendown()

    r1.goto(r1.xcor() + 20, r1.ycor() - 40)

    r1.goto(r1.xcor() - 20, r1.ycor())

    r1.goto(r1.xcor() - 10, r1.ycor() + 30)

    r1.goto(r1.xcor() - 10, r1.ycor())

    r1.goto(r1.xcor(), r1.ycor() - 30)

    r1.goto(r1.xcor() - 20, r1.ycor())

    r1.setheading(0)
    r1.penup()
    r1.goto(r1.xcor() + 30, r1.ycor() + 50)
    r1.pendown()
    r1.fillcolor("white")
    r1.circle(10, 180)

    r1.fillcolor("gold")
    r1.forward(10)

    r1.left(90)
    r1.forward(20)

    r1.left(90)
    r1.forward(10)

    r1.end_fill()
    #r1.hideturtle()

def draw_r2():
    r2 = Turtle()
    r2.color("black")
    r2.fillcolor("yellow")

    r2.pensize(5)

    r2.penup()
    r2.goto(90, 0)
    r2.pendown()

    r2.begin_fill()

    r2.setheading(90)
    r2.forward(80)

    r2.right(90)
    r2.forward(50)

    r2.goto(r2.xcor() + 10, r2.ycor() - 10)

    r2.right(90)
    r2.forward(20)

    r2.goto(r2.xcor() - 20, r2.ycor() - 20)

    r2.setheading(90)
    r2.forward(40)

    r2.left(90)
    r2.forward(20)

    r2.left(90)
    r2.forward(30)

    r2.left(90)
    r2.forward(20)

    r2.goto(r2.xcor() + 20, r2.ycor() - 40)

    r2.goto(r2.xcor() - 20, r2.ycor())

    r2.goto(r2.xcor() - 10, r2.ycor() + 30)

    r2.setheading(180)
    r2.forward(10)

    r2.left(90)
    r2.forward(30)

    r2.right(90)
    r2.forward(20)

    r2.end_fill()
    #r2.hideturtle()

def draw_o():
    o = Turtle()
    o.color("black")
    o.fillcolor("gold")

    o.pensize(5)

    o.penup()
    o.goto(170, 0)
    o.pendown()

    o.begin_fill()

    o.goto(o.xcor() - 20, o.ycor() + 20)

    o.goto(o.xcor(), o.ycor() + 40)

    o.goto(o.xcor() + 20, o.ycor() + 20)

    o.goto(o.xcor() + 20, o.ycor())

    o.goto(o.xcor() + 20, o.ycor() - 20)

    o.goto(o.xcor(), o.ycor() - 40)

    o.goto(o.xcor() - 20, o.ycor() - 20)

    o.goto(o.xcor() - 20, o.ycor())

    o.end_fill()

    o.fillcolor("white")
    o.penup()
    o.goto(o.xcor(), o.ycor() + 20)
    o.pendown()

    o.begin_fill()

    o.setheading(90)
    o.forward(40)

    o.right(90)
    o.forward(20)

    o.right(90)
    o.forward(40)
    
    o.right(90)
    o.forward(20)

    o.end_fill()
    #r2.hideturtle()



# This block should ALWAYS be the LAST part of your code
if __name__ == "__main__":
    draw_n()
    draw_a1()
    draw_v()
    draw_a2()
    draw_r1()
    draw_r2()
    draw_o()
    exitonclick()