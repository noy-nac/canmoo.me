from turtle import Turtle

def my_function():
    bob = Turtle()
    bob.shape("turtle")
    bob.color("red")
    
    bob.screen.mainloop()

# This block should ALWAYS be the LAST part of your code
if __name__ == "__main__":
    my_function()