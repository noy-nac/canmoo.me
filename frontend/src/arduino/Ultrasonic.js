import Unipage from "../templates/Unipage";

export default function Ultrasonic() {

    return (<Unipage
        content={
            <>
                <h2>Range Finder!</h2>

                <p>
                    The range finder will consist of two circuits, one for our button and one for the ultrasonic sensor.
                </p>

                <h3>Materials</h3>
                <p>
                    <ul>
                        <li>1 ardunio</li>
                        <li>1 breadboard</li>
                        <li>6-8 male-to-male wires</li>
                        <li>1 resistor</li>
                        <li>1 button</li>
                        <li>1 ultrasonic sensor</li>
                    </ul>
                </p>

                <p>
                    Reminder that, in general, <strong>it is NOT a good idea to make changes to your circuit while the Arduino is plugged in!!!!!!</strong>
                </p>

                <h3>Step 1: Install a Button</h3>
                <p>Golden rule: Does it make a <b>complete</b> circuit?</p>
                <p>
                    <ol>
                        <li>Run power from <b>3.3V</b> to one half of the button.</li>
                        <li>When the button is pushed, it should send voltage to a numbered pin on the Arduino</li>
                        <li>Don't forget a pull-down resistor connected to GND!!!!</li>
                        <p><img src="/arduino_imgs/pulldown.png" height="250 px" /></p>
                        <p>
                            When the button is pressed and released, there is leftover charge stuck in the side of the button connected to the signal pin.
                        </p>
                        <p>
                            The pulldown resistor allows this excess charge to dissipate to GND.
                            If we don't let the residual charge pass to ground, it will look to the Arduino like our button is always on because the signal pin will always be reading HIGH.
                        </p>
                    </ol>
                </p>

                <h3>Step 2: Install the Ultrasonic Sensor</h3>
                <p>
                    <img src="/arduino_imgs/ultrasonic.png" height="250 px" />
                </p>
                <p>
                    The ultrasonic sensor has four pins, two to make a complete circuit and two to commuinicate with the board.
                </p>
                <p>
                    <ol>
                        <li>Run power from <b>5V</b> to the Vcc (Voltage closed circuit) pin.</li>
                        <li>Connect the GND pin to GND on the Arduino.</li>
                        <li>Connect the Trig and Echo pins to two digital I/O pins on the Arduino.</li>
                        <ul><li>We will use these pins to communicate with the ultrasonic sensor.</li></ul>
                    </ol>
                </p>

                <p>
                    <img src="/arduino_imgs/echolocation.png" height="250 px" />
                </p>

                <h3>Code</h3>
                <p>
                    Written in class!
                </p>
                <p>
                    Solution: <a href="/arduino_code/ultrasonic_sensor.txt" download>ultrasonic_sensor.txt</a>
                </p>

                <h3>Diagram</h3>
                <p>Try not to look! Try to use the instructions above!</p>
                <p>
                    <img src="/arduino_imgs/ultrasonic_sensor.jpg" height="400 px" />
                </p>
            </>
        }
    />);

}