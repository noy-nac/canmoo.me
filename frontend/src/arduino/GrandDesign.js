import Unipage from "../templates/Unipage";

export default function Buzzer() {

    return (<Unipage
        content={
            <>
                <h2>Grand Design Challenge</h2>
                <p>
                    Hello everyone, as we approach the end of the class, our last circuit build will be a project that will be done in pairs with a partner of your choosing.
                    This webpage is to help you brainstorm!
                </p>

                <p>
                    <h3><a href="https://forms.gle/ZBETntpHzXfmurJU8" target="_blank">CODE SAVE</a></h3>
                    <h3><a href="https://docs.google.com/spreadsheets/d/1CeLsJ70ZtYk0dFnQf9MMj9_s2zk1CKuH9zzIPMws538/edit?usp=sharing" target="_blank">RESPONSES</a></h3>
                </p>

                <h3>Online Resources</h3>
                <p>
                    <ul>
                        <li><a href="https://projecthub.arduino.cc/" target="_blank">Arduino Project Hub</a></li>
                        <li><a href="https://www.instructables.com/Arduino-Projects/" target="_blank">Autodesk Instructables</a></li>
                    </ul>
                </p>

                <h3>Resources from Class (use these!!!!!)</h3>
                <p>
                    Look below to find the projects we've built in class. You may use code and take inspiration from ANY build we've done in class.
                </p>
                <p>
                    <ol>
                        <li>
                            <a href="/arduino/buzzer" target="_blank">Morse Code Beeper</a>
                            <ul>
                                <li>Pizoelectreic buzzer</li>
                                <li>Button</li>
                            </ul>
                        </li>
                        <li>
                            <a href="/arduino/speaker" target="_blank">Making Music</a>
                            <ul>
                                <li>Potentiometer</li>
                                <li>Speaker</li>
                                <li>Button</li>
                            </ul>
                        </li>
                        <li>
                            <a href="/arduino/stoplight" target="_blank">Stoplight & Crosswalk</a>
                            <ul>
                                <li>LEDs</li>
                                <li>Button</li>
                            </ul>
                        </li>
                        <li>
                            <a href="/arduino/ultrasonic" target="_blank">Range Finder</a>
                            <ul>
                                <li>Ultrasonic Sensor</li>
                            </ul>
                        </li>
                        <li>
                            <a href="/arduino/servo" target="_blank">Servo Fan</a>
                            <ul>
                                <li>Servo Motor</li>
                            </ul>
                        </li>
                    </ol>
                </p>
            </>
        }
    />);

}