import Unipage from "../templates/Unipage";

export default function Speaker() {

    return (<Unipage
        content={
            <>
                <h2>Making Music!</h2>

                <h3>Materials</h3>
                <p>
                    <ul>
                        <li>1 ardunio</li>
                        <li>1 breadboard</li>
                        <li>8 male-to-male wires</li>
                        <li>1 resistor</li>
                        <li>1 button</li>
                        <li>1 speaker (looks like a larger buzzer)</li>
                        <li>1 potentiometer (looks like a knob)</li>
                    </ul>
                </p>

                <p>
                    Reminder that, in general, <strong>it is NOT a good idea to make changes to your circuit while the Arduino is plugged in!!!!!!</strong>
                </p>


                <h3>Build Diagram</h3>
                <p>
                    Notice there are three independent circuits in the build: the button, the potentiometer, and the speaker.
                    The circuits communicate by reading and writing the Arduino's I/O pins.
                </p>
                <p>We will write code to facilitate this communication. Thus, the code will be the glue that holds everything together.</p>
                <p><img height="600 px" src="/arduino_imgs/modulated_speaker.jpg" /></p>

                <h3>Build Assembly</h3>
                <p>
                    <ol>
                        <li>Mount the button on the bridge separating the two halves of the breadboard.</li>
                        <li>Place the potentiometer somewhere in the middle of the board.</li>
                        <li>Place the speaker on the far end of the breadboard.</li>
                        <br/>
                        <li>Connect the <b>left</b> half of the button to the positive bus and then to 5V.</li>
                        <li>Connect the <b>right</b> half of the button to pin 13.</li>
                        <li>The right half should have a pull-down resistor connected to the negative bus and then to GND.</li>
                        <br />
                        <h4>Potentiometer Pinout</h4>
                        <p>
                            The potentiometer is a variable resistor a.k.a. a voltage splitter.
                            As the knob turns the wiper along the track, the current experiences more resistance. This causes the voltage on the signal pin to drop.
                        </p>
                        <p><img height="250 px" src="/arduino_imgs/potentiometer.png"/></p>
                        <li>Connect the first pin on the potentiometer to the positive bus.</li>
                        <li>Connect the second (middle) pin on the potentiometer to pin A0.</li>
                        <li>Connect the last pin on the potentiometer to negative bus.</li>
                        <br/>
                        <li>Connect either pin on the speaker to pin A1. A speaker does not have polarity.</li>
                        <li>Connect the other speaker pin to GND.</li>
                    </ol>
                </p>

                <h3>Digital vs. Analog I/O Pins</h3>
                <p>Digital pins (#2-13) can send and recieve signals as ON or OFF, HIGH or LOW, 0 or 1 only (think binary).</p>
                <p>Analog I/O pins (#A0-A5) can send and recieve signals with gradations in value.</p>
                <p><img height="400 px" src="/arduino_imgs/digital_vs_analog.png"/></p>

                <h3>Code</h3>
                <p>
                    Copy the following code into a new window in Arduino IDE: <a href="/arduino_code/modulated_speaker.txt" download>modulated_speaker.txt</a>
                </p>

            </>
        }
    />);

}