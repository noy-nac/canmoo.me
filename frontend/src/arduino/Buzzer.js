import Unipage from "../templates/Unipage";

export default function Buzzer() {

    return (<Unipage
        content={
            <>
                <h2>Morse Code Buzzer</h2>
                <h3>Materials</h3>
                <p>
                    <ul>
                        <li>1 ardunio</li>
                        <li>1 breadboard</li>
                        <li>4 male-to-male wires</li>
                        <li>1 resistor</li>
                        <li>1 button</li>
                        <li>1 pizoelectric buzzer</li>
                    </ul>
                </p>

                <p>
                    Reminder that, in general, <strong>it is NOT a good idea to make changes to your circuit while the Arduino is plugged in!!!!!!</strong>
                </p>


                <h3>Reminder: Breadboard Wiring!!!</h3>
                <p>Breadboards have wires underneath. Look at the silver parts!!!</p>
                <p><img height="300 px" src="/arduino_imgs/breadboard.png" /></p>

                <h3>READ ME: Build Assembly Instructions</h3>
                <p>
                    <ol>
                        <img height="200 px" src="/arduino_imgs/button.png" />
                        <li>Mount the button on the bridge separating the two halves of the breadboard. Legs A & B should be on one side of the bridge and legs C & D on the other.</li>
                        <li>Connect the resistor from the upper-right leg of the button (leg B) to the negative bus.</li>
                        <br/>
                        <li>Mount the buzzer on an unused portion of the breadboard.</li>
                        <li>Run a wire from between button leg B and resistor to the positive end of the buzzer.</li>
                        <li>Run a wire from the negative end of the buzzer to the negative bus.</li>
                        <br/>
                        <li>Connect the upper-left leg (leg A) of the button to the 5V pin on the Arduino.</li>
                        <li>Connect the negative bus to the GND pin on the Arduino.</li>
                    </ol>
                </p>

                <h3>Build Diagram</h3>
                <p>
                    When complete, your build should have the same <b>circuit</b> as the diagram below.
                    <b>The color, placement, and such of the wiring can and SHOULD be different than the diagram.</b>
                </p>
                <p>Prove that your build creates a <b>complete circuit</b> by tracing your finger without lifting from the positive end of the circuit (5V) to the negative end (GND).</p>
                <p>We will discuss the purpose of the resistor in the circuit more tomorrow.</p>
                <p><img height="450 px" src="/arduino_imgs/buzzer.png" /></p>

                <h3>Morse Code</h3>
                <p><img height="350 px" src="/arduino_imgs/morse_code.png" /></p>

                <p>
                    Using Morse code and your buzzer, transmit the following messages:
                    <ul>
                        <li>"SOS"</li>
                        <li>"I love you"</li>
                        <li>Your name, pet's name, school's name</li>
                    </ul>
                </p>

                <h2>Extension: Making Music!</h2>

                <p>
                    Replace the buzzer with a proper speaker and control the output sound using a potentiometer.
                    You will need to upload the code below to the arduino in order for this extension to work.
                </p>

                <h3>Materials</h3>
                <p>
                    <ul>
                        <li>1 ardunio</li>
                        <li>1 breadboard</li>
                        <li>8 male-to-male wires</li>
                        <li>1 resistor</li>
                        <li>1 button</li>
                        <li>1 speaker (looks like a larger buzzer)</li>
                        <li>1 potentiometer (looks like a knob)</li>
                    </ul>
                </p>

                <h3>Diagram</h3>
                <p><img height="600 px" src="/arduino_imgs/modulated_speaker.jpg" /></p>

                <h3>Code</h3>
                <p>
                    Copy the following code into a new window in Arduino IDE: <a href="/arduino_code/modulated_speaker.txt" download>modulated_speaker.txt</a>
                </p>
            </>
        }
    />);

}