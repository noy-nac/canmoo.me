import Unipage from "../templates/Unipage";

export default function Stoplight() {

    return (<Unipage
        content={
            <>
                <h2>Stoplight!</h2>

                <h3>Materials</h3>
                <p>
                    <ul>
                        <li>1 ardunio</li>
                        <li>1 breadboard</li>
                        <li>7 male-to-male wires</li>
                        <li>1 resistor</li>
                        <li>1 green LED</li>
                        <li>1 yellow LED</li>
                        <li>1 red LED</li>
                    </ul>
                </p>

                <p>
                    Reminder that, in general, <strong>it is NOT a good idea to make changes to your circuit while the Arduino is plugged in!!!!!!</strong>
                </p>


                <h3>Requirements / Assembly</h3>
                <p>
                    <ol>
                        <li>Set up the three LEDs on the breadboard.</li>
                        <li>Make each LED part of a circuit.</li>
                        <ul>
                            <li><b>IMPORTANT:</b> Which end of the LED is the positive end?</li>
                        </ul>
                        <li>The positive end of each circuit should be connected to a digital pin (pins 1-13) on the arduino.</li>
                        <li>The negative ends of each circuit should be connected to GND.</li>
                        <li><strong>Each circuit must have a resistor to stop the LED from receiving too much current!</strong></li>
                    </ol>
                </p>

                <h3>Diagram</h3>
                <p>Only an example. Yours may (and should) look different. Try not to copy!</p>
                <p><img height="600 px" src="/arduino_imgs/stoplight_green.jpg" /></p>

                <h3>Code</h3>
                <p>
                    Written in class!
                </p>
                <p>
                    Solution code: <a href="/arduino_code/stoplight.txt">stoplight.txt</a>
                </p>


                <h2>Extension: Crosswalk!</h2>

                <p>Modify the build and code so that a <b>button</b> controls the changing of the green light to yellow, then red.</p>

                <p><img height="600 px" src="/arduino_imgs/crosswalk_red.jpg" /></p>
                <p>
                    Solution code: <a href="/arduino_code/crosswalk.txt">crosswalk.txt</a>
                </p>

            </>
        }
    />);

}