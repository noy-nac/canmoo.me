import Unipage from "../templates/Unipage";

export default function Servo() {

    return (<Unipage
        content={
            <>
                <h2>Servo Fan!</h2>

                <h3>Materials</h3>
                <p>
                    <ul>
                        <li>1 ardunio</li>
                        <li>3 male-to-male wires</li>
                        <li>1 servo motor</li>
                    </ul>
                </p>

                <p>
                    Reminder that, in general, <strong>it is NOT a good idea to make changes to your circuit while the Arduino is plugged in!!!!!!</strong>
                </p>

                <h3><b>Instructions: READ ME</b></h3>
                <p>Golden rule: Does it make a <b>complete</b> circuit?</p>
                <p>
                    This build will consist of simply wiring the servo to the Arduino. Connect the red wire to power (5V), the brown wire to ground (GND), and the yellow wire to a digital signal pin.
                </p>

                <h3>Code</h3>
                <p>
                    Written in class!
                </p>
                <p>
                    Solution: <a href="/arduino_code/servo.txt" download>servo.txt</a>
                </p>
                <p>
                    A for loop uses a control variable to iterate through code a number of times.
                    <ol>
                        <li>The first part, "Initalization", is the initial value of the control variable. This code runs once before the loop begins.</li>
                        <li>The second part, "Test Condition", is the loop invariant. As long as this condition is True, the loop will continue iterating. This condition is checked AFTER each iteration of the loop and the update statement.</li>
                        <li> The thrid part, "Updation", is how the control variable changes each iteration. It runs AFTER each iteration of the loop, but BEFORE the test condition.</li>
                    </ol>

                </p>
                <p>
                    <img src="/arduino_imgs/forloops.png" height="250 px" />
                </p>
                <p>If you finish, play around with the delay numbers and for loop parameters to change how the fan moves!</p>

                <h3>Build Diagram</h3>
                <p>
                    <img src="/arduino_imgs/servo.png" height="300 px" />
                </p>

                <h3>Extension</h3>
                <p>Add a button or potentiometer to control the servo.</p>

                <p>
                    Build examples for button ONLY and for potentiometer ONLY. You will need to add this on to the servo build.
                </p>
                <p><img src="/arduino_imgs/btnpot.jpg" height="400 px" /></p>
                <p>
                    Example code using button and also a potentiometer: <a href="/arduino_code/modulated_speaker.txt" download>modulated_speaker.txt</a>
                    <ul><li>Hint: If you choose a button, use an if statement. If you choose the potentiometer, use the code to read an analog pin.</li></ul>
                </p>
            </>
        }
    />);

}