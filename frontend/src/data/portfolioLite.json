[
  {
    "major": "A",
    "minor": "0",
    "name": "Profile Information",
    "prompt": [
      "In this section, you will provide the portfolio evaluator with a context for you as a student and pre-service teacher."
    ],
    "prose": [
      ""
    ],
    "artifacts": []
  },
  {
    "major": "A",
    "minor": "1",
    "name": "Academic Summary",
    "prompt": [
      "The academic record helps to provide the portfolio evaluator with an understanding of how far you have progressed in the content area. Your academic record should include all UT coursework, all transfer course work, and all grades."
    ],
    "prose": [
      "I attached my academic summary as artifact a1-coursework.pdf."
    ],
    "artifacts": [
      "a1-coursework.pdf"
    ]
  },
  {
    "major": "A",
    "minor": "2",
    "name": "Resume",
    "prompt": [
      "This document presents your work and educational background. The audience is a principal or other professional who hires teachers.",
      "Your resume should be up-to-date and include your UTeach field experiences at all levels (with an indication that these were short-term, not full-time employment) and any UTeach internship experience"
    ],
    "prose": [
      "I attached my resume as artifact a2-resume.pdf."
    ],
    "artifacts": [
      "a2-resume.pdf"
    ]
  },
  {
    "major": "A",
    "minor": "3",
    "name": "Teaching Philosophy",
    "prompt": [
      "State the major concepts and guiding principles that shape your views on being a successful teacher. You might choose to address such questions as:",
      "What led you to become a teacher?",
      "What do you hope to accomplish as a teacher?",
      "What educational experiences (either as a student or pre-service teacher) have had the greatest impact on you, and why?"
    ],
    "prose": [
      "I attached my teaching philosophy as artifact a3-philosophy.pdf."
    ],
    "artifacts": [
      "a3-philosophy.pdf"
    ]
  },
  {
    "major": "A",
    "minor": "4",
    "name": "Cover Letter",
    "prompt": [
      "State your future goals for employment in the form of a letter that accompanies a resume to a potential employer. This letter should be tailored to the job you seek, demonstrating to a future employer that you are a good fit for the school."
    ],
    "prose": [
      "I attached my cover letter as artifact a4-cov.pdf."
    ],
    "artifacts": [
      "a4-cov.pdf"
    ]
  },
  {
    "major": "A",
    "minor": "5",
    "name": "Ethics and Professionalism",
    "prompt": [
      "Reflect on the importance of ethics in teaching. Refer to a specific standard from the \"Code of Ethics and Standard Practices for Texas Educators.\" Describe the ways in which this standard might apply in your future classroom."
    ],
    "prose": [
      "Ethics in teaching is important because teachers hold a position of public trust in society, and are furthermore in a position of power over vulnerable members of society—specifically minors, but therein people from all walks of life with myriad experiences growing up, at home, or who may be physically or otherwise disabled.",
      "I attach the artifact a5-ethics.pdf, which is my signed copy of the code of ethics for Texas teachers. I produced this artifact during a class discussion reflecting on each standard. For this proficiency, I reflect on Standard 3.4: \"The educator shall not exclude a student from participation in a program, deny benefits to a student, or grant an advantage to a student on the basis of race, color, gender, disability, national origin, religion, family status, or sexual orientation.\"",
      "One way I can apply this standard is to create a safe space in my classroom such that students feel free to be themselves and bullying isn't tolerated.",
      "Furthermore, the layout of materials and student areas can help create inclusive spaces for students with disabilities. This could include, for example, wide aisles between desks as well as handout and other materials located at a reachable height.",
      "I also read this standard as extending to systemic inequities. Many students come from under-resourced households in terms of technology, computer access, and internet access. Since computer science may involve some programming and technology use, one way I could address this inequity would be to survey students on access to computers and the internet at home. Then I could use the results to develop supports for such students or alter instructional decisions. For example, I could make myself and computer/internet spaces more available to on projects, grant extensions as necessary, or develop instructional activities that are not reliant on computer use at home.",
      "Finally, I can include as many students as possible by making my lessons relevant to the interests and backgrounds of all students. Such lessons will encourage greater participation."
    ],
    "artifacts": [
      "a5-ethics.pdf"
    ]
  },
  {
    "major": "B",
    "minor": "0",
    "name": "Equity and Inclusive Design",
    "prompt": [
      "In this section, you describe the steps taken to ensure that all of the students you teach have engaging and meaningful learning opportunities. Demonstrate your awareness of the needs of diverse groups of learners and provide evidence that you consider these needs throughout the entire process of developing a learning experience."
    ],
    "prose": [
      ""
    ],
    "artifacts": []
  },
  {
    "major": "B",
    "minor": "1",
    "name": "Student Diversity",
    "prompt": [
      "Demonstrate an awareness of student diversity while preparing lessons, presenting lessons, and assessing students.",
      "Include a discussion of how you create an inclusive and accessible learning environment for students from various backgrounds, who have different interests, ability levels, genders, students for whom English is not a first language, or students who legally require accommodations and/or modifications."
    ],
    "prose": [
      "I demonstrated an awareness of student diversity by engaging students through multiple sensory pathways, implementing multi-pronged and choice-based assessments, and making digital copies of lesson materials available for use with accessibility services.",
      "To show how I implemented multiple ways of assessing students, I attached artifact b1-div-assesment.pdf. The assessment includes writing Java code, story-telling, and presenting. In the presentation, there are options for an oral presentation, a poster presentation, and a video presentation.",
      "I included artifact b1-mult-engag.pdf (slides 13-14) to show how I engaged students through multiple senses, in this case when giving directions. For this lesson, students had some code to play around with. On the slides, I included visual representations to aid in my oral explanation of the interface. Then I stepped through using the code book (mentioned in the lesson plan on page 4 of artifact d1-lp2.pdf). Finally, there were written instructions in the code book itself. The point is I provided students multiple ways to get the information they needed. Students might have trouble holding oral instructions in their head, so I included them also written down. At the same time, students might have trouble putting an oral/written explanation alone into context, so I include pictures and demo it.",
      "Artifact b1-lp.pdf on pages 4-5 lists accommodations for students in my CI teach, some of which were suggested to us by our mentor teacher. Particularly important was making our materials available digitally, such as on Google Docs, because then they can be used with accessibility services and translated as needed."
    ],
    "artifacts": [
      "b1-div-assesment.pdf",
      "b1-lp.pdf",
      "b1-lp2.pdf",
      "b1-mult-engag.pdf"
    ]
  },
  {
    "major": "C",
    "minor": "0",
    "name": "Classroom Environment and Managment",
    "prompt": [
      "In this section, you will demonstrate how you create a safe and supportive learning environment that fosters high expectations for the success of all students."
    ],
    "prose": [
      ""
    ],
    "artifacts": []
  },
  {
    "major": "C",
    "minor": "1",
    "name": "Participation and Connection",
    "prompt": [
      "Explain how you design lessons that encourage all students to participate, that connect the content to the interests and experiences of your students, and that make learning assessable and meaningful for students of all ability levels."
    ],
    "prose": [
      "To encourage all students to participate, I use community-building techniques like circles. During my PBI teach with Ms. Halley-Walker's class, we went around and talked about what we were looking forward to over the summer (See artifact c1-connection-1.pdf, slide 52). Creating a community in the classroom can help students feel more confident to speak up in a discussion or if they need help.",
      "Another way I make learning accessible for all students is using techniques like think-pair-share (TPS). In think-pair-share, students first take a moment to think about the question themselves, then discuss with their neighbors, and finally each group has the opportunity to share with the class. The \"think\" part of TPS gives students time to reflect on how they themselves are making sense of the content. The \"pair\" part of TPS gives students the opportunity to hear or share their ideas in a low-stakes setting and to further incorporate ideas they hadn't thought of in their sense-making. And finally, the \"share\" part of TPS lets students articulate a developed and rich explanation. In my experience, it is also common for students to acknowledge the contributions of others but who may be shy in sharing their ideas. (See artifact c1-connection-1.pdf, slides 11-13).",
      "To help make content interesting and relevant to students' experience, I involved students in choosing lesson topics. For example, for my PBI teach with Ms. Halley-Walker's class, I surveyed students' interest in possible lesson topics during my observations (See artifact c1-connection-1.pdf, slide 5).",
      "Another way I make content interesting to students is to use situations in their daily lives as entry points for motivating the content itself. For example, in my second Step 2 teach with Mr. Borders' class, I asked students if their parents looked through their phone, how they felt about it if they did, and if there was ever a situation they would absolutely want to keep information secret to motivate an exploration of cryptography. I then tied this discussion to topics like the dark web (Tor), and its history of being funded by the US government for human rights purposes, and the Apple-FBI encryption dispute to motivate a broader discussion of the place of cryptography in society. (See artifact c2-connection-2.pdf, slides 6-10)."
    ],
    "artifacts": [
      "c1-connection-1.pdf",
      "c2-connection-2.pdf"
    ]
  },
  {
    "major": "C",
    "minor": "2",
    "name": "Safety",
    "prompt": [
      "State explicitly how you consistently maintain a safe learning environment for students, both emotionally and physically. Include a lesson plan in which you specifically discuss safety with your students."
    ],
    "prose": [
      "To help maintain emotional safety during a lesson on homelessness, I made Austin homelessness resources available to students. This came on feedback from my mentor teacher (See artifact c2-em-safety.pdf, slide 34). I actually also included a short write up of some more general safety considerations because I wanted to make sure that, since we were using interviews of real people, that they would be respected (See artifact c2-safety-con.pdf; the associated lesson plan is artifact c2-lp.pdf).",
      "I maintain a physically safe classroom by keeping students in view. Additionally, when lessons involve possibly dangerous materials (e.g., hot glue gun), I establish procedures to safely use the materials. For example, for a cardboard construction table, I modeled how to use an electric rotating cutter safely. In the hot glue gun example, this could include stations at the front of the room, so the teacher can monitor usage."
    ],
    "artifacts": [
      "c2-em-safety.pdf",
      "c2-lp.pdf",
      "c2-safety-con.pdf"
    ]
  },
  {
    "major": "D",
    "minor": "0",
    "name": "Lesson Design",
    "prompt": [
      "Inquiry is an approach to teaching that involves students exploring concepts or ideas in order to create new understandings.  The purpose of this section is for you to describe and document your process for developing lessons that promote student learning through inquiry. Your reflections should explain how your inquiry lesson is connected to state and national standards. You should also discuss the ways in which your lesson design uses the experiences and perspectives of the learner to support students taking an active role in the construction of their own knowledge."
    ],
    "prose": [
      ""
    ],
    "artifacts": []
  },
  {
    "major": "D",
    "minor": "1",
    "name": "Inquiry Design and Assessment",
    "prompt": [
      "Document your process for planning and developing a learning experience designed to promote student learning through inquiry. Explain how you develop assessments to evaluate and demonstrate the student's grasp of the lesson material in relation to state or national standards. Also explain how you assessed the validity of the resources and student activities for this lesson."
    ],
    "prose": [
      "To design an inquiry-based lesson, I find it effective to perform the inquiry myself. As part of that process, I identify the key insights and the evidence that made those insights clear. Then I think about how to connect the insights to a model or vocabulary. Finally, I map this process to the format of a class.",
      "I attached two artifacts, d1-steg.pdf and d1-passwords.pdf, which represent the process I described above. In both labs, I start with an activity. In the steganography lab, students use an online tool to hide an image in another. There's a \"hidden bits\" slider which lets students see what happens to the final image as the slider is increased. In the passwords lab, students try out some common passwords and see how strong they're ranked. Students try to guess what kinds of passwords will be strong.",
      "After the initial experience, students have more experiences that highlight key concepts, as well as group discussions. Students record what they noticed and answer discussion questions to help make sense of what the experiences mean. I think in implementation, if I were to do this lesson again, I would do one lab per day and have some whole class discussions to help with my formative assessment and organization. When I taught this lesson, I originally had groups rotating to stations, but I think bifurcating the class made it harder to keep students on track and flexibly respond to questions.",
      "To relate lessons and assessment to state/national standards, I use a backwards design process. I start with the standards, use that to extract learning objectives, then connect those objectives to different means of assessment. Finally, knowing the specific learning objectives and assessment, I work the lesson to specifically target those proficiencies. This can be seen in my PBI teach in Ms. Halley-Walker's class (See artifact d1-unpack.pdf), where first I unpacked the standards related to objects. Then I used those connections to build the project requirements, and finally I outlined the project. I think my implementation here has many opportunities for improvement. First, I should actually state the standards I'm unpacking (they were 6(E) understand concepts of object-oriented design; CodeHS 2.2 Creating and Storing Objects (Instantiation)). Second, I could include probing questions and possible follow-ups depending on how students respond. Finally, I could do a better job of making clear what are the skills (the how) and what is the knowledge (the what/why) I want to impart."
    ],
    "artifacts": [
      "d1-passwords.pdf",
      "d1-steg.pdf",
      "d1-unpack.pdf"
    ]
  },
  {
    "major": "D",
    "minor": "2",
    "name": "Technology",
    "prompt": [
      "Discuss how you have used or plan to use technology to create and enhance the learning environment. Include an assessment of the appropriateness of that technology for reaching your instructional goals."
    ],
    "prose": [
      "I attached my main reflection as artifact d2-tech-refl.pdf. I also attached my lesson plan and slides as artifacts d2-tech-less.pdf and d2-tech-slides.pdf."
    ],
    "artifacts": [
      "d2-tech-less.pdf",
      "d2-tech-refl.pdf",
      "d2-tech-slides.pdf"
    ]
  },
  {
    "major": "E",
    "minor": "0",
    "name": "Lesson Implementation",
    "prompt": [
      "In this section, you will demonstrate how you stimulate interest in your content and elicit students' sustained participation in learning activities through inquiry and the use of technology."
    ],
    "prose": [
      ""
    ],
    "artifacts": []
  },
  {
    "major": "E",
    "minor": "1",
    "name": "Questioning and Assessment",
    "prompt": [
      "Describe and provide evidence of your questioning and assessment techniques. Include evidence of how you used assessment to respond flexibly to students during instruction. Include a discussion of how your assessment strategy or instrument effectively measured the learning objectives and how you used assessment data to revise and improve the lesson."
    ],
    "prose": [
      "I attached artifact e1-q&assesment.pdf, which includes an example (pages 3-5 starting from 1:19:22 in the transcript) of changing how I was questioning in response to a formative assessment. In this example, I thought a student had demonstrated understanding of a concept, and I attempted to ask a leading question to the implication of their understanding. When the student replied \"I don't know\" I had to back up and reassess. I asked the student to walk me through their understanding, and then I extended their understanding to a concrete example. Overall, I have mixed feelings about this interaction because of my pivot to a direct explanation. I feel I could have better preserved the student's opportunity to discover the principle for themselves by asking some leading questions about the example I put forward. My main takeaway is it's okay to back up and reassess if necessary, and that a fruitful discussion is still possible even if it seems like you hit a roadblock.",
      "For my step 2 teach about codes and ciphers, I collected some physical work and graded it. I attached the rubric as artifact e1-rubric.pdf and the results (just the handout, no pre/post test) as artifact e1-grades.pdf. Overall, the results told me I was trying to do too much in the lesson. Not only did students go to multiple stations, but also I had an extension activity that involved passing messages in code to tell a story. Students had trouble finishing the handout during the activity. I would in retrospect spend the full day exploring the different codes, and move the passing code activity to the next day so students would have more time to finish."
    ],
    "artifacts": [
      "e1-grades.pdf",
      "e1-q&assesment.pdf",
      "e1-rubric.pdf"
    ]
  },
  {
    "major": "E",
    "minor": "2",
    "name": "Meaningful Lesson Experience",
    "prompt": [
      "Describe an inquiry-based lesson that you have actually implemented.  Provide evidence showing that all students engaged in meaningful learning experiences, such as: making predictions, gathering data, creating their own explanations or models from data, and communicating those explanations."
    ],
    "prose": [
      "For my CI teach, I implemented two inquiry-based lessons, one about passwords and one about steganography. The passwords lab had students use a password strength tool to test strong and weak passwords of their own design. The students' goal was to figure out what made strong passwords strong. The steganography lab had students play around with a tool that hid an image in another. Then they could play with a slider to see how it changed the final image. Then they played around with a color picker to see how changing each significant digit changed the overall color and compared this result to the result from the tool. I attached both labs as e2-pass.pdf and e2-steg.pdf.",
      "I attached artifact e2-stu-work.pdf, which has some examples showing students engaged in meaningful lesson experiences. For example, students came up with example passwords and sorted them into \"strong\" and \"weak\" categories (e.g., pp. 1-2), so the data surrounding strong and weak passwords was generated by students. Furthermore, students made predictions about what made a password strong (e.g., p. 1). Likewise, students also made predictions about how a steganography tool was working (e.g., p. 3). Then following the exploration, students responded to questions to articulate their own explanations for the phenomenon they observed (e.g., p. 4)."
    ],
    "artifacts": [
      "e2-pass.pdf",
      "e2-steg.pdf",
      "e2-stu-work.pdf"
    ]
  },
  {
    "major": "F",
    "minor": "0",
    "name": "Content Knowledge",
    "prompt": [
      "In this section, you demonstrate YOUR knowledge in the subject matter you will be teaching. Your reflections should describe broad and current knowledge. Include work you have completed in your university courses. Be sure to thoroughly describe the work and how it demonstrates your proficiency."
    ],
    "prose": [
      ""
    ],
    "artifacts": []
  },
  {
    "major": "F",
    "minor": "1",
    "area": "ALL",
    "name": "Historical Importance",
    "prompt": [
      "Show how you bring out the historical importance of your subject material, its contribution to large ideas, and its significance in today’s society.  Include a brief discussion in your reflection/analysis of the importance of helping your students understand the context of the information you present."
    ],
    "prose": [
      "I attached artifact fa1-ai.pdf, a research paper on artificial intelligence I wrote four years ago! I cover historical trends on pages 1-3 and social impacts on pages 8-11.",
      "It's important to help students understand the context of what they learn because students are, or will eventually be, actors in society. Context allows students to place what they learn in the society in which they live. One example in computer science is the amount of power programmers can have over people's lives just through the code they write, which gives rise to the discussion of how to act ethically as a programmer with the power of the algorithm. Analyzing historical trends and context allows students to notice patterns between the now and the then. One example is the boom and bust cycle of AI, which somewhat resembles the stagnation of a couple years ago with the recent emergence and hype around ChatGPT."
    ],
    "artifacts": [
      "fa1-ai.pdf"
    ]
  },
  {
    "major": "F",
    "minor": "2",
    "area": "ALL",
    "name": "Model",
    "prompt": [
      "Generate a model of a natural phenomenon, or an engineered product or process, or describe an already existing model and evaluate how well the model represents the situation."
    ],
    "prose": [
      "For my research methods inquiry IV, I attempted to model the relationship between the flow rate of a dripping faucet and the periodicity of the drip. Try this at home! When a faucet is first dripped at a low flow rate, drops will fall at a constant rate. As the flow rate is increased, however, the drops begin to fall in pairs drip-drip, drip-drip, drip-drip. While before the period of dripping was constant for a particular flow rate, at a higher flow rate there are actually two periods. One period for the time from drip-drips to the next drip-drip and one period for the time between a single drip-drip. So in a sense the single period underwent a bifurcation, it split into two, as the flow rate increased.",
      "This kind of behavior motivates the idea that the periodicity of the drip could be modeled by the logistic map. The logistic map is a recurrence relation where the (x+1)th value is given by the product of the xth value times 1 minus the same times a constant r for 0 <= r <= 4 and 0 <= xth value <= 1. (See fa2-pres.pdf, slide 4).",
      "As described in artifact fa2-model.pdf, I constructed a system to capture a spigot dripping at different rates and collected drop data by recording the sound of the drops hitting a pan. I then wrote a program to extract the time each drop hit the pan, plotted on the x-axis, and the time between each drop, plotted on the y-axis.",
      "Overall, my results were consistent with the idea that a recurrence relation similar to the logistic map could model a dripping faucet. At low flow rates, I observed a constant period that slowly increased as the pressure dropped. At high flow rates, I observed chaotic behavior, as well as period doublings and quadruplings, as is seen in the logistic map.",
      "However, I had trouble managing the sheer amount of data my experiment generated. I extracted data from 25 recordings, and it worked out to a couple megabytes of text data. Consequently, I had trouble compiling disparate graphs to put together a bifurcation diagram to compare with that of the logistic map."
    ],
    "artifacts": [
      "fa2-model.pdf",
      "fa2-pres.pdf"
    ]
  },
  {
    "major": "F",
    "minor": "3",
    "area": "ALL",
    "name": "Topic Connections",
    "prompt": [
      "Describe a topic in the subject area and describe the connections with prerequisite topics, future topics, and other subjects."
    ],
    "prose": [
      "I attached artifact fa3-mind-map.pdf, which is a mind map of topic connections centered on conditional execution. Roughly speaking the top half of the map includes connections to more pre-req topics, like logical operators, logic, comparison, sequential execution, boolean, set theory, and natural language. Again roughly speaking, the bottom half includes more future topics, like looping, if/else, if/else if/else, loop types, recursion, base cases, induction, iterables, and lazy execution.",
      "Furthermore, I attached artifact fa3-int.pdf, which is a type up of the through-line from logical operators to if statements to if/else if/else. I wrote this up while working at a project-based summer camp for some students who were trying to include some complex logic in their projects."
    ],
    "artifacts": [
      "fa3-int.pdf",
      "fa3-mind-map.pdf"
    ]
  },
  {
    "major": "F",
    "minor": "4",
    "area": "MATH",
    "name": "Formal/Informal Reasoning",
    "prompt": [
      "State a mathematical theorem or conjecture and apply informal mathematical reasoning and a formal proof of the theorem or conjecture.",
      "Evidence must include a statement about the particular type of proof chosen for the theorem or conjecture (e.g., direct proof, proof by contradiction, existence proof, uniqueness proof, contrapositive and equivalent forms, proof by mathematical induction, etc.) and why that method of proof is most appropriate."
    ],
    "prose": [
      "I attached a proof (see artifact fm1-dist.pdf) that the greatest common divisor (gcd) and least common multiple (lcm) operations obey the distributive laws; that is, a*(b+c) = (a*b)+(a*c) and a+(b*c) = (a+b)*(a+c) for natural numbers a, b, and c, where x*y denotes gdc(x,y) and x+y denotes the lcm(x,y).",
      "I chose this proof because it wasn't a theorem I had to prove for an assignment. I thought that it might be true and proved it on my own based on the informal intuition I developed about the nature of unique prime factorizations.",
      "The informal intuition that led to this proof comes from the idea that if the prime factorization of a natural number a has at least every factor in the prime factorization of another natural number b, then it's almost like the number a contains the number b. I also did a formal proof of this theorem, where instead of \"a contains b\" I showed b divides a. The key informal idea is that divisibility can be like containing. This let me start drawing pictures and map those pictures to the idea of divisibility. See artifact fm1-inform.pdf for an explanation of this informal reasoning.",
      "Thinking about lcm and gcd in this context, I realized they were kind of like union (lcm) and intersection (gdc) in set theory. By definition, lcm(x,y) is the smallest number divisible by both x and y, while gcd(x,y) is the greatest number that divides both x and y. Going back to artifact fm1-inform.pdf, you can think of lcm as shrinking D until it covers A and B and nothing else, while you can think of gcd as expanding C until it covers as much of the overlap between A and B as possible. I'd shown in a previous class that union and intersection were distributive so I tried to show it for gcd and lcm.",
      "For the formal proof (see artifact fm1-dist.pdf), I ended up using a direct proof. I thought this method most appropriate because I had previously made direct connections between the concepts of lcm and gcd and prime factorization such that I could manipulate the result algebraically, so I figured I could use that algebraic manipulation to show distributivity holds."
    ],
    "artifacts": [
      "fm1-dist.pdf",
      "fm1-inform.pdf"
    ]
  },
  {
    "major": "F",
    "minor": "5",
    "area": "MATH",
    "name": "Multiple Representations",
    "prompt": [
      "Describe a mathematical concept that can be represented in multiple ways and articulate the connections between its representations in clear, expository prose."
    ],
    "prose": [
      "I attached an explanation of some connections between different representations in the concept of modularity as artifact fm2-mod.pdf. I cover modularity in the real world, clocks, division remainders, modular functions and graphs, equivalence classes, and generalized modularity in group theory. It's handwritten.",
      "Also found this assignment on all the multiple representations of the handshake problem. Attached as artifact fm2-hand.pdf. It's also handwritten."
    ],
    "artifacts": [
      "fm2-hand.pdf",
      "fm2-mod.pdf"
    ]
  },
  {
    "major": "F",
    "minor": "6",
    "area": "CS",
    "name": "Computational Problem",
    "prompt": [
      "State a computational problem and an algorithm for solving it. Apply both informal and formal techniques to evaluate the algorithm in terms of its clarity, efficiency, and correctness. Describe the benefit of being able to apply both formal and informal reasoning to the same problem."
    ],
    "prose": [
      "I consider a problem stated in one of my algo homeworks, problem 3 on pages 5-7 of artifact fcs1-algo.pdf. The homework contains a formal write up. I supply the informal write up here.",
      "Right off the bat, I think the most helpful way to apply informal reasoning to the problem is to draw a picture so we know what all the variables mean in context (see artifact fcs1-pic.pdf).",
      "To construct an algorithm that solves this problem, imagine you go along each location and decide to either put a router there or not. What does placing a router do? Immediately, we get the users associated from that location. However, now we can't get any users from any preceding router within m feet of the current location.",
      "To formalize this thought, let opt(i) denote the optimal (maximum) number of users after placing routers (or not) up to position i. opt(i) is either opt(i-1) if no router is placed or opt(B(i))+p_i where B(i) is the furthest position j preceding i such that m <= |d_i-d_j|. So opt(i) = max(opt(i-1), opt(B(i))+p_i). What this formalization is doing is taking the informal strategy of asking what happens when we place a router and applying it with an eye towards the most optimal solution.",
      "To actually code this solution, I describe the algorithm in artifact fcs1-algo.pdf. The approach is to first determine B(i) for all i, then to calculate opt(i) for all i. Since there are n possible positions and computing any B(i) or opt(i) is O(1), we would expect computing B(i) and opt(i) to both take O(n) time. Two O(n) operations together is still O(n) because computation time is still proportional to n.",
      "To show the algorithm is correct, we can ask what would the optimal algorithm do and show at each step our algorithm does the same thing as the optimal algorithm. When i=1, then both our algorithm and the optimal algorithm place a router at position 1, so they both maximize the number of users (p_1).",
      "Now we can ask what happens if the two algorithms match for all positions up to but not including i. Then we can show the two algorithms will also match at i because opt(i) = max(opt(i-1), opt(B(i))+p_i) and both i-1 and B(i) are less than i.",
      "But then since we showed in the base case, the two algorithms match, and we also showed that if they match up to i-1 then they also match at i, then they must match for all positions.",
      "One benefit of applying both formal and informal reasoning to a computational problem is that the informal reasoning can help you step through the problem like a computer, which assists in the development of computational thinking. Translating that informal reasoning into formal reasoning helps build connections between computing and mathematical concepts like induction."
    ],
    "artifacts": [
      "fcs1-algo.pdf",
      "fcs1-pic.pdf"
    ]
  },
  {
    "major": "F",
    "minor": "7",
    "area": "CS",
    "name": "Software System",
    "prompt": [
      "Describe a scenario or problem that can be improved or solved by the creation of a software system, and formalize that system using multiple representations, including: descriptive prose, a graphical representation (UML, flowchart, etc.), code or pseudo-code."
    ],
    "prose": [
      "I describe a personal project I built to help my friends and I play Dungeons & Dragons online.",
      "It is an online whiteboard/battlemat. The way it works is as follows: The Dungeon Master (DM) visits the website URL to create a new room. In the upper-right hand corner, the DM has access to a share button which copies a player link to the clipboard. The display consists primarily of a grid which the DM can draw on, while players see the drawing update in real time.",
      "In addition to the description above, I formalized this project by providing some source code samples. Artifact fcs2-op-code.js.txt contains code for handling user-initiated operations on the html canvas. Artifact fcs2-uml.png is a class hierarchy diagram for this code. Artifact fcs2-server.py.txt contains the backend server code used to handle concurrent connections and broadcast canvas updates to users.",
      "Finally, I included two devlogs I made while working on the project this summer, which include my commentary and demonstration of two key technical challenges. (1) server-client communication and (2) hosting on AWS. They are artifacts fcs2-devlog-2.mp4 and fcs2-devlog-3.mp4. Together, they are about 5 minutes long.",
      "Full source code: https://gitlab.com/noy-nac/whiteboard/",
      "Sadly, I had to take down my EC2 backend because I ran out of AWS free-tier credits. So whiteboard.canmoo.me is just a blank screen now."
    ],
    "artifacts": [
      "fcs2-devlog-2.mp4",
      "fcs2-devlog-3.mp4",
      "fcs2-op-code.js.txt",
      "fcs2-server.py.txt",
      "fcs2-uml.png"
    ]
  }
]