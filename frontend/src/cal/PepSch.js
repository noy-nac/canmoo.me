
export default function PepSch(props) {
	return (
		<>
			<h4 style={{ textAlign: "center" }}>
				A Day - Pep Rally
			</h4>
			<table className="table table-bordered" style={{ textAlign: "center" }}>
				<thead>
					<tr style={{ fontWeight: 600 }}><td>Period</td><td>Time</td><td>Content</td><td>Date AT Assumes</td></tr>
				</thead>
				<tbody>
					<tr className="table-warning">
						<td>1st</td>
						<td>9:05 - 10:30</td>
						<td>AP CS A</td>
						<td>1/29/2024</td>
					</tr>
					<tr>
						<td>Pep Rally</td>
						<td>10:40 - 11:20</td>
						<td>-</td>
						<td></td>
					</tr>
					<tr>
						<td>2nd</td>
						<td>11:25 - 12:50</td>
						<td>AP CS Principles</td>
						<td>-</td>
					</tr>
					<tr>
						<td>Lunch</td>
						<td>12:50 - 1:35</td>
						<td>-</td>
						<td>-</td>
					</tr>
					<tr>
						<td>3rd</td>
						<td>1:40 - 3:05</td>
						<td>Introduction to CS</td>
						<td>-</td>
					</tr>
					<tr>
						<td>4th</td>
						<td>3:10 - 4:35</td>
						<td>Conference</td>
						<td>-</td>
					</tr>
				</tbody>
			</table>
			<h4 style={{ textAlign: "center" }}>
				B Day - Pep Rally
			</h4>
			<table className="table table-bordered" style={{ textAlign: "center" }}>
				<thead>
					<tr style={{ fontWeight: 600 }}><td>Period</td><td>Time</td><td>Content</td><td>Date AT Assumes</td></tr>
				</thead>
				<tbody>
					<tr>
						<td>5th</td>
						<td>9:05 - 10:30</td>
						<td>AP CS Principles</td>
						<td>-</td>
					</tr>
					<tr>
						<td>Pep Rally</td>
						<td>10:40 - 11:20</td>
						<td>-</td>
						<td>-</td>
					</tr>
					<tr className="table-warning">
						<td>6th</td>
						<td>11:25 - 12:50</td>
						<td>Introduction to CS</td>
						<td>1/29/2024</td>
					</tr>
					<tr>
						<td>Lunch</td>
						<td>12:50 - 1:35</td>
						<td>-</td>
						<td>-</td>
					</tr>
					<tr>
						<td>7th</td>
						<td>1:40 - 3:05</td>
						<td>Engineer Your World</td>
						<td>-</td>
					</tr>
					<tr>
						<td>8th</td>
						<td>3:10 - 4:35</td>
						<td>Conference</td>
						<td>-</td>
					</tr>
				</tbody>
			</table>
		</>
	);
}