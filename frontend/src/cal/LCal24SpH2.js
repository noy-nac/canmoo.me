
import "./LivingCalendar.css"

export default function LCal24SpH2(props) {
	//

	const headers = [
		"Week", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday"
	];
	const body = [
		[
			[<>Wk. 12</>],
			[<>Mar. 18 -- Day A1</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/1kk5GJD9pOF7qWmWpsztyDFUd3Iwt8fKHuQp0znvtNU0/edit?usp=sharing">Merge Sort</a></>,
				<>- Designing Merge Sort</>,
				<>- <a href="https://github.com/noy-nac/fast-sorts">Fast Sorts</a></>,
			],
			[<>19 -- Day B1 </>,
				<>CS 1 -- <a href="https://docs.google.com/presentation/d/1JCK1FFp1YRzf7GvGXFLI9C2X2CyLUEHFg4jAncpqsws/edit?usp=sharing">Classes</a></>,
				<>- Start Using Classes</>,
			],
			[<>20 -- Day A2 </>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/1EzAvwipa-nzPiXKu_LgKO2x6euyidbGwsw_u7uZucJE/edit?usp=sharing">Merge Sort</a> + <span className="cal-tst">QUIZ</span></>,
				<>- Work day</>,
				<>- <a href="https://github.com/noy-nac/fast-sorts">Fast Sorts</a></>,
			],
			[<>21 -- Day B2 </>,
				<>CS 1 -- <a href="https://docs.google.com/presentation/d/1oNO0B2LQ7ieZAR3Xaq8X6sl2jJII_UKcfm-wt8BN0NU/edit?usp=sharing">Classes II</a></>,
				<>- Finish Using Classes</>,
			],
			[<>22 -- Day B3 </>,
				<>CS 1 -- <a href="https://docs.google.com/presentation/d/1ED_NmW8_QR4IfGOOhQQwByfhUHEguTvr5x4AAtsgjsU/edit?usp=sharing">Classes</a></>,
				<>- Start Crash</>,
				<>- <a href="/class_code/frogger.py">frogger.py</a></>,
				<>- <a href="/class_code/frog.py">frog.py</a></>,
				<>- <a href="/class_code/car.py">car.py</a></>,
			],
		],
		[
			[<>Wk. 13</>],
			[<>Mar. 25 -- Day A3</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/1YsP0JcFsXOYLqP4XQiV1XZ_hX4kSSi2okfRVKQlVizM/edit?usp=sharing">Quicksort</a> + <span className="cal-tst">QUIZ</span></>,
				<>- <a href="https://github.com/noy-nac/fast-sorts">Fast Sorts</a></>,
			],
			[<>26 -- Day B4</>,
				<>CS 1 -- <a href="https://docs.google.com/presentation/d/1UET7LXRdgBbomZpF-nXrhts_2cU4bQ-IgvYnxjrkGrw/edit?usp=sharing">Classes</a></>,
				<>- Continue Crash</>,
			],
			[<>27 -- Day A4</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/1O7oaN9KWP83hDuZ0uffXNzQeFHDmIKVrzgyoBrqkrlY/edit?usp=sharing">Interfaces</a></>,
				<>- <a href="https://github.com/noy-nac/arraylist">APrrayList</a></>,
				<>- <a href="https://github.com/noy-nac/items-inventory">ItemInventory</a></>,
				<>- <a href="https://docs.oracle.com/javase/8/docs/api/java/util/List.html">List</a>, <a href="https://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html">ArrayList</a>, <a href="https://docs.oracle.com/javase/8/docs/api/java/util/LinkedList.html">LinkedList</a></>,
				<><div className="cal-alert">IRP proposal due 11:59pm</div></>,
			],
			[<>28 -- Day B5</>,
				<>CS 1 -- <a href="https://docs.google.com/presentation/d/1voIwvh5_iTDmBM2UFmLRqJmAP2ijCtYAB01VT8Xc4iM/edit?usp=sharing">Classes</a></>,
				<>- Continue Crash</>,
			],
			[<>29</>,
				<><div className="cal-holi">HOLIDAY</div></>
			],
		],
		[
			[<>Wk. 14</>],
			[<>Apr. 1 -- Day A5</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/1ZuHBzfITO1PKpQRlFnJ4CQ_k4vEC-GMAVETqbdy3pcY/edit?usp=sharing">Interfaces</a> + IRPs</>,
				<>- <a href="https://github.com/noy-nac/arraylist">APrrayList</a></>,
			],
			[<>2 -- Day B6</>,
				<>CS 1 -- <a href="https://docs.google.com/presentation/d/1voIwvh5_iTDmBM2UFmLRqJmAP2ijCtYAB01VT8Xc4iM/edit?usp=sharing">Classes</a></>,
				<>- Continue Crash</>,
			],
			[<>3 -- Day A6</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/1tFCoqUVc1jXrUCtydxRWZdSXtQqNaLs8l86L7VJ4_g8/edit?usp=sharing">Top-down Merge Sort</a> + <span className="cal-tst">QUIZ</span></>,
				<>- <a href="/class_code/RecursionTrees.java" download>RecursionTrees.java</a></>,
			],
			[<>4 -- Day B7</>,
				<>CS 1 -- <a href="https://docs.google.com/presentation/d/1aQ3hLjWtaLnSpPsKTfkWv0SYwufzbmuuInu6F9jtvbo/edit?usp=sharing">Classes</a></>,
				<>- Continue Crash</>,
			],
			[<>5 -- Day A7</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/1ERPUX7CQiEbLrstVj8ezKePKbNz0OaL2XfwpudLUjqE/edit?usp=sharing">Bottom-up Merge Sort</a></>,
			],
		],
		[
			[<>Wk. 15</>],
			[<>Apr. 8 -- Day A8</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/1pGPZHXsTnj3NtF992dsEOKJRKDMGtdXFBy_qNm2Q1tU/edit?usp=sharing">Interfaces, IRPs,</a> + <span className="cal-tst">QUIZ</span></>,
				<><div className="cal-sp">TOTAL SOLAR ECLIPSE @ 1:36pm</div></>,
				<>- <a href="https://www.timeanddate.com/eclipse/in/usa/austin?iso=20240408">Eclipse Info</a></>,
				<><div className="cal-alert">Fast Sorts due 11:59pm</div></>,
			],
			[<>9 -- Day B8</>,
				<>CS 1 -- <a href="https://docs.google.com/presentation/d/1fWoJeMihpCtpUOvC1_B6Otcd3MNcXqncPRlX1FCXRcc/edit?usp=sharing">Classes</a></>,
				<>- Crash playlist</>,
				<>- Probably the last day for Crash</>,
				<><div className="cal-sp">English I EOC</div></>,
			],
			[<>10</>,
				<><div className="cal-holi">STAFF DEV.</div></>
			],
			[<>11 -- Day B9</>,
				<>CS 1 -- <a href="https://docs.google.com/presentation/d/1OmZQkNl6YHDCJMTjqUI81ooNSFLx8FtsvMKeQfYQu6c/edit?usp=sharing">Brainstorming</a></>,
				<><div className="cal-sp">English II EOC</div></>,
			],
			[<>12 -- Day A9</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/15i5nJ-YWhY_se5_-aqC6yBuY-TwpDKQ9Xa22XnQBlkA/edit?usp=sharing">Singly and Doubly Linked Lists</a> + the same <span className="cal-tst">QUIZ</span></>,
				<>- <a href="https://docs.oracle.com/javase/8/docs/api/java/util/LinkedList.html">LinkedList</a></>,
			],
		],
		[
			[<>Wk. 16</>],
			[<>Apr. 15 -- Day A10</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/1_XXFdmpJK8OwIlkJUd4T7uXnFvCZX2WYavJ9YBbNILQ/edit?usp=sharing">LinkedLists II</a> + <span className="cal-tst">QUIZ</span></>,

				<><div className="cal-alert">Reflection 1 due 11:59pm</div></>,
				<><div className="cal-alert">APrrayList due 11:59pm</div></>,
			],
			[<>16 -- Day B10</>,
				<>CS 1 -- Game Work Day</>,
				<><div className="cal-sp">Biology EOC</div></>,
			],
			[<>17 -- Day A11</>,
				<>CS A -- IRP Work Day</>,
				//<>CS A -- Hash Tables</>,
				//<>- <a href="https://docs.oracle.com/javase/8/docs/api/java/util/HashMap.html">HashMap</a></>,

				<><div className="cal-sp">US History EOC</div></>,
			],
			[<>18 -- Day B11</>,
			],
			[<>19 -- Day B12</>,
			],
		],
		[
			[<>Wk. 17</>],
			[<>Apr. 22 -- Day A12</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/15d0DoMfvZoN5HoWyU_G5sCeGNAKer6Noija8AX5ojTU/edit?usp=sharing">Binary Search Trees</a></>,
				//<>CS A -- Queues & Stacks</>,
				<>- <a href="https://docs.oracle.com/javase/8/docs/api/java/util/TreeMap.html">TreeMap</a></>,

				//<>- <a href="https://docs.oracle.com/javase/8/docs/api/java/util/Queue.html">Queue</a></>,
				//<>- <a href="https://docs.oracle.com/javase/8/docs/api/java/util/Stack.html">Stack</a></>,
				<><div className="cal-alert">Reflection 2 due 11:59pm</div></>,
			],
			[<>23 -- Day B13</>,
				<><div className="cal-sp">Algebra I EOC</div></>,
			],
			[<>24 -- Day A13</>,
				<>CS A -- IRP Work Day + <span className="cal-tst">QUIZ</span></>,
				//<>CS A -- Priority Queues + <span className="cal-tst">QUIZ</span></>,
				//<>- <a href="https://docs.oracle.com/javase/8/docs/api/java/util/PriorityQueue.html">PriorityQueue</a></>,
			],
			[<>25 -- Day B14</>,
			],
			[<>26 -- Day A14</>,
			],
		],
		[
			[<>Wk. 18</>],
			[<>Apr. 29 -- Day A15</>,
				<>CS A -- Hash Tables + <span className="cal-tst">QUIZ</span></>,
				<>- <a href="https://docs.oracle.com/javase/8/docs/api/java/util/HashMap.html">HashMap</a></>,
				<><div className="cal-alert">Reflection 3 due 11:59pm</div></>,
			],
			[<>30 -- Day B15</>,
			],
			[<>May 1 -- Day A16</>,
				<>CS A -- ?? </>,
			],
			[<>2 -- Day B16</>,
			],
			[<>3 -- Day B17</>,
			],
		],
		[
			[<>Wk. 19</>],
			[<>May 6 -- Day A17</>,
				<>CS A -- AP Exam Prep</>,
				<><div className="cal-sp">AP US Gov in AM</div></>,
				<><div className="cal-sp">AP Chem in PM</div></>,
				<><div className="cal-alert">Reflection 4 due 11:59pm</div></>,
			],
			[<>7 -- Day B18</>,
				<><div className="cal-sp">AP Human Geo in AM</div></>,
				<><div className="cal-sp">AP Stats in PM</div></>,
			],
			[<>8 -- Day A18</>,
				<>CS A -- AP Exam Prep</>,
				<><div className="cal-sp">AP Eng Lit in AM</div></>,
				<><div className="cal-sp">AP CS A in PM</div></>,
			],
			[<>9 -- Day B19</>,
				<><div className="cal-sp">AP Psych in PM</div></>,
			],
			[<>10 -- Day A19</>,
				<><div className="cal-sp">AP US, Euro Hist in AM</div></>, 
				<><div className="cal-sp">AP Spanish in PM</div></>,
				<><div className="cal-sp">AT graduates on Saturday</div></>
			],
		],
		[
			[<>Wk. 20</>],
			[<>May 13 -- Day A20</>,
				<>CS A -- Presentations</>,
				<><div className="cal-sp">SENIORS ONLY 1st (CS A), 2nd per. Exams</div></>,
				<><div className="cal-sp">AP Calc AB/BC in AM</div></>,
			],
			[<>14 -- Day B20</>,
				<><div className="cal-sp">SENIORS ONLY 5th, 6th (CS 1) per. Exams</div></>,
				<><div className="cal-sp">AP Eng Lang in AM</div></>,
				<><div className="cal-sp">AP Phy C in PM</div></>,
			],
			[<>15 -- Day A21</>,
				<>CS A -- Presentations</>,
				<><div className="cal-sp">SENIORS ONLY 3rd, 4th per. Exams</div></>,
				<><div className="cal-sp">AP French, World Hist in AM</div></>,
				<><div className="cal-sp">AP CS P in PM</div></>,
			],
			[<>16 -- Day B21</>,
				<><div className="cal-sp">SENIORS ONLY 7th, 8th per. Exams</div></>,
				<><div className="cal-sp">AP Latin in PM</div></>,
			],
			[<>17 -- <span className="cal-sp">C DAY</span></>,
				<><div className="cal-alert">End of grading cycle</div></>,
				<><div className="cal-alert">Reflection 5 due 11:59pm</div></>,
			],
		],
		[
			[<>Wk. 21</>],
			[<>May 20</>,
				<><div className="cal-alert">1st (CS A), 2nd per. Exams</div></>,
				<><div className="cal-sp">Early release @ 1:35pm</div></>,
			],
			[<>21</>,
				<><div className="cal-alert">5th, 6th (CS 1) per. Exams</div></>,
				<><div className="cal-sp">Early release @ 1:35pm</div></>,
			],
			[<>22</>,
				<><div className="cal-alert">3rd, 4th per. Exams</div></>,
				<><div className="cal-sp">Early release @ 1:35pm</div></>,
			],
			[<>23</>,
				<><div className="cal-alert">7th, 8th per. Exams</div></>,
				<><div className="cal-sp">Early release @ 1:35pm</div></>,
			],
			[<>24</>,
				<><div className="cal-alert">TEACHER LAST DAY</div></>
			],
		],

	]

	return (
		<>
			<table className="table table-bordered">
				<thead>
					<tr>
						{
							headers.map(colHead => <th className="mc-head" scope="col">{colHead}</th>)
						}
					</tr>
				</thead>
				{
					body.map(bodyRow =>

						<tbody>
							<tr>
								{
									bodyRow.map((rc, i, a) => {
										return (<td>
											{
												rc.map((value, index, array) => {
													return index > 0
														? <div className="mc-data">{value}</div>
														: <div className="mc-title">{value}</div>
												})
											}
										</td>)
									})
								}

							</tr>
						</tbody>
					)
				}
			</table>
		</>
	);

}