
import "./LivingCalendar.css"

export default function LCal24SpH1(props) {

	const headers = [
		"Week", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday"
	];
	const body = [
		[
			[<>Wk. 1</>],
			[<>Jan. 8</>, <><div className="cal-holi">STAFF DEV.</div></>],
			[<>9 -- Day B1 </>],
			[<>10 -- Day A1 </>],
			[<>11 -- Day B2 </>],
			[<>12 -- Day A2 </>],
		],
		[
			[<>Wk. 2</>],
			[<>Jan. 15</>, <><div className="cal-holi">MLK HOLIDAY</div></>],
			[<>16 -- Day B3</>],
			[<>17 -- Day A3</>],
			[<>18 -- Day B4</>],
			[<>19 -- Day A4</>],
		],
		[
			[<>Wk. 3</>],
			[<>Jan. 22 -- Day A5</>,
				<>- Observing</>,
			],
			[<>23 -- Day B5</>,
				<>- Observing</>,
			],
			[<>24 -- Day A6</>,
				<>- Observing</>,
			],
			[<>25 -- Day B6</>,
				<>- Observing</>,
			],
			[<>26 -- Day B7</>,
				<>- Observing</>,
				<><div className="cal-sp">Apprentice teacher leaving at 1pm</div></>,
			],
		],
		[
			[<>Wk. 4</>],
			[<>Jan. 29 -- Day A7</>,
				<>CS A -- Abstract Classes</>,
				<>- Finish Train</>,
				<>- Begin <a href="https://github.com/noy-nac/zoo">Zoo</a></>,
				<>- <a href="https://aisdblend.instructure.com/courses/452737/discussion_topics/4117770">Announcement</a></>,
				<>- <a href="https://docs.google.com/presentation/d/1PAx9Djb16pJGP30FF9ClhUQDMTSGIYnbK80FCoQUakE/edit?usp=sharing">About Me</a></>,
				<><span className="cal-alert">HW</span> -- Read the <a href="https://github.com/noy-nac/zoo/blob/main/README.md">README</a></>,
			],
			[<>30 -- Day B8</>,
				<>CS 1 -- 2D Arrays</>,
				<>- Finish TicTacToe</>,
				<>- <a href="https://docs.google.com/presentation/d/1PAx9Djb16pJGP30FF9ClhUQDMTSGIYnbK80FCoQUakE/edit?usp=sharing">About Me</a></>,
			],
			[<>31 -- Day A8</>,
				<>CS A -- Abstract Classes + <span className="cal-tst">QUIZ</span></>,
				<>- Continue <a href="https://github.com/noy-nac/zoo">Zoo</a>
			</>],
			[<>Feb. 1 -- Day B9</>,
				<>CS 1 -- <a href="https://docs.google.com/presentation/d/19FJlSSJqWRuUGOIG4PBsdVntIiQKHFkkTIx08FFaPbA/edit#slide=id.p">2D Arrays</a></>,
				<>- Begin <a href="https://github.com/noy-nac/zork">Zork</a></>,
				<>- Play Zork</>,
				<>- <a href="https://docs.google.com/document/d/1mdcvKjzllBDnlel2wiX_1cYmlfT65oziarRXPMr7X3s/edit?usp=sharing">Zork design doc</a></>,
				<>- <a href="https://aisdblend.instructure.com/courses/452728/discussion_topics/4118623">Access design docs here</a></>,
			],
			[<>2 -- Day A9</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/19FJlSSJqWRuUGOIG4PBsdVntIiQKHFkkTIx08FFaPbA/edit#slide=id.g2b57ec653e8_0_46">Abstract Classes</a></>,
				<>- Continue <a href="https://github.com/noy-nac/zoo">Zoo</a></>,
				<>- Look inside Zoo.java</>,
				<>- Zoo.rand, tick(), at(x,y)</>,
				<>- <a href="https://docs.google.com/document/d/1vFZHFcpIKMyAL05SLuHT8B2wpF_elx4yBfFke7l8sYM/edit?usp=sharing">Benefits of Abstract Classes</a></>,
				<>- <a href="https://aisdblend.instructure.com/courses/452737/discussion_topics/4118938">OCP & SOLID</a></>,
			],
		],
		[
			[<>Wk. 5</>],
			[<>Feb. 5 -- Day A10</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/19FJlSSJqWRuUGOIG4PBsdVntIiQKHFkkTIx08FFaPbA/edit#slide=id.g2b602b1eef3_0_0">Abstract Classes & Open-closed Principle</a></>,
				<>- Continue <a href="https://github.com/noy-nac/zoo">Zoo</a></>,
				<><div className="cal-sp">Student teacher orientation 1-3pm</div>
			</>],
			[<>6 -- Day B10</>,
				<>CS 1 -- <a href="https://docs.google.com/presentation/d/19FJlSSJqWRuUGOIG4PBsdVntIiQKHFkkTIx08FFaPbA/edit#slide=id.g2b6d9f7d206_0_5">Function Decomposition</a></>,
				<>- Continue <a href="https://github.com/noy-nac/zork">Zork</a></>,
				<>- Start programming Zork</>,
				<>- <a href="https://docs.google.com/document/d/1vFZHFcpIKMyAL05SLuHT8B2wpF_elx4yBfFke7l8sYM/edit?usp=sharing">Exit Ticket</a></>,
			],
			[<>7 -- Day A11</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/19FJlSSJqWRuUGOIG4PBsdVntIiQKHFkkTIx08FFaPbA/edit#slide=id.g2b706c24751_0_41">Abstract Classes</a> + <span className="cal-tst">GROUP QUIZ</span></>,
				<>- Continue <a href="https://github.com/noy-nac/zoo">Zoo</a></>,
			],
			[<>8 -- Day B11</>,
				<>CS 1 -- <a href="https://docs.google.com/presentation/d/19FJlSSJqWRuUGOIG4PBsdVntIiQKHFkkTIx08FFaPbA/edit#slide=id.g2b706c24751_0_117">2D Arrays</a></>,
				<>- Moving in 4 directions</>,
				<>- Continue <a href="https://github.com/noy-nac/zork">Zork</a></>,
			],
			[<>9</>,<><div className="cal-holi">STAFF DEV.</div></>],
		],
		[
			[<>Wk. 6</>],
			[<>Feb. 12 -- Day A12</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/1VTR53WtBz2_lZzAESKn2-X8luUCUOqJs3L0TO7zJ21s/edit?usp=sharing">Binary Search</a></>,
				<>- The Movie Scene Problem</>,
				<>- Implement <a href="https://github.com/noy-nac/9-1-search">Search</a></>,
				<>- <a href="https://aisdblend.instructure.com/courses/452737/discussion_topics/4120663">Hint</a></>,
				<><span className="cal-alert">HW</span> -- Finish Search</>,
			],
			[<>13 -- Day B12</>,
				<>CS 1 -- <a href="https://docs.google.com/presentation/d/1pqFlGT5cUXziQwfI0HevYhYJY2oYPnPodnZWG7YEjJQ/edit?usp=sharing">2D Arrays</a></>,
				<>- Game loop & testing move</>,
				<>- Continue <a href="https://github.com/noy-nac/zork>">Zork</a></>,
			],
			[<>14 -- Day A13</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/1-K52eUpYZYOnekYVJHIZnG8aFbIVKJKw1mWcXcwyI5o/edit?usp=sharing">Big O</a> + <span className="cal-tst">QUIZ</span></>,
				<>- Work on Zoo for 30 minutes</>,
				<>- Run time of Linear vs Binary Search</>,
				<>- <a href="/class_code/BigO.java">BigO.java</a></>,
				<>- <a href="https://aisdblend.instructure.com/courses/452737/discussion_topics/4121255">Announcement</a></>,
			],
			[<>15 -- Day B13</>,
				<>CS 1 -- <a href="https://docs.google.com/presentation/d/1k4f5mPyUjXQIO4rrg1lyzI0Lq-pyY7q6OtNaGpDSX9Q/edit">2D Arrays</a></>,
				<>- Continue <a href="https://github.com/noy-nac/zork">Zork</a></>,
				<>- Demo: Item pick up</>,
				<>- <a href="/class_code/ItemPickup.py">ItemPickup.py</a></>,
				<><div className="cal-alert">Check Merge requests &rarr; Feedback</div></>,
			],
			[<>16 -- Day A14</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/1sVGdKmo234b9oDk5H6rOt_dC81r7W23608ele2W3KA4/edit?usp=sharing">Big O & Sorting</a></>,
				<>- Work on Zoo for 30 minutes</>,
				<>- Invent a Sort</>,
				<>- Class stats <a href="https://forms.gle/ZYJ7k9xo7grY7zYD8">survey</a></>,
				<><div className="cal-alert">Last in-class day to work on Zoo</div></>,
			],
		],
		[
			[<>Wk. 7</>],
			[<>Feb. 19</>,<><div className="cal-holi">HOLIDAY</div></>],
			[<>20 -- Day B14</>,
				<>CS 1 -- 2D Arrays</>,
				<>- Continue <a href="https://github.com/noy-nac/zork">Zork</a></>,
			],
			[<>21 -- Day A15</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/1sVGdKmo234b9oDk5H6rOt_dC81r7W23608ele2W3KA4/edit#slide=id.g2bafec73312_0_69">Quadratic Sorts</a> + <span className="cal-tst">GROUP QUIZ</span></>,
				<>- Class stats <a href="https://forms.gle/ZYJ7k9xo7grY7zYD8">survey</a></>,
				<>- <a href="https://docs.google.com/spreadsheets/d/1xsf3sI_xlh1zdY0Cqyyjz0Ky5W2qNGheY4UvNDqWpr8/edit?usp=sharing">Responses</a></>,
				<>- Start <a href="https://github.com/noy-nac/quadratic-sorts">Quadratic Sorts</a></>,
				<>- <a href="https://classroom.github.com/a/1IfFZpC3">GitHub Classroom</a></>,
			],
			[<>22 -- Day B15</>,
				<>CS 1 -- <a href="https://docs.google.com/presentation/d/1pJxF_9O9IvC_nK9VGfM2l_pMxtQNrYF1nH06rh_CCMw/edit?usp=sharing">2D Arrays</a></>,
				<>- Continue <a href="https://github.com/noy-nac/zork">Zork</a></>,
			],
			[<>23 -- Day B16</>,
				<>CS 1 -- <a href="https://docs.google.com/presentation/d/1EM4fQmfSMw14oddcUQEFUmvzkwfWtVqxm1CDIPwSsLI/edit?usp=sharing">2D Array</a></>,
				<>- Finish <a href="https://github.com/noy-nac/zork">Zork</a></>,
				<><div className = "cal-alert">Zork HARD DEADLINE</div></>,
			],
		],
		[
			[<>Wk. 8</>],
			[<>Feb. 26 -- Day A16</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/1lndqVKeQHv1UmyC0ppxywzqOlasp6153GlruH7wh3Pg/edit?usp=sharing">Sorting + Big O</a></>,
				<>- Special warm up problem</>,
				<>- Work day</>,
				<>- Big O chart</>,
				<><div className="cal-alert">Search soft deadline</div></>,
			],
			[<>27 -- Day B17</>,
				<>CS 1 -- Presentations</>,
				<>- Zork Presentations</>,
				<>- 5 minutes per group</>,
				<>- 3 Slides: Your World, How to win/lose, Reflection</>,
				<>- Demo</>,
			],
			[<>28 -- Day A17</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/1d4dDRyFsBClf_X0qalH7HX8BTF4bam2vm_IDC5ga8pw/edit?usp=sharing">Big O</a> + <span className="cal-tst">QUIZ</span></>,
				<>- Practical <a href="https://docs.google.com/document/d/1a1a2SOocQg_NYOSi99qfyEqFwaQx2FjnJr1_o1RQGIc/edit?usp=sharing">Big O Lab</a></>,
				<>- <a href="https://docs.google.com/document/d/11JQuM50_Ok0QvU7UfPXoT4dR6ewoD8l7aoUCcH6HLL4/edit?usp=sharing">Functions for Lab</a></>,
				<><div className="cal-alert">Zoo soft deadline</div></>,
			],
			[<>29 -- Day B18</>,
				<>CS 1 -- <a href="https://docs.google.com/presentation/d/11cpde2TBz0I6Ua1H-7slNAi_9qcq5wL84hF6kgDo0tk/edit?usp=sharing">Turtle Graphics</a></>,
				<>- Setup computers</>,
				<>- <a href="https://github.com/noy-nac/turtle-explore">Exploring Python Turtle</a></>,
				<>- Sharing activity</>,
			],
			[<>Mar. 1 -- Day A18</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/1eVdphlL75ceJtTN6buSS9gKAUnddBQd0i7xBci3TMi4/edit?usp=sharing">Big O + Intro to Merge Sort</a></>,
				<>- Practical <a href="https://docs.google.com/document/d/1a1a2SOocQg_NYOSi99qfyEqFwaQx2FjnJr1_o1RQGIc/edit?usp=sharing">Big O Lab</a></>,
				<>- <a href="https://docs.google.com/document/d/11JQuM50_Ok0QvU7UfPXoT4dR6ewoD8l7aoUCcH6HLL4/edit?usp=sharing">Functions for Lab</a></>,
				<>- Intro to <a href="https://youtu.be/a43TAeOo_A4">Merge Sort Video</a> (first 5 min)</>,
			],
		],
		[
			[<>Wk. 9</>],
			[<>Mar. 4 -- Day A19</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/1SeNM0GEzOKEFnQbU3aiJExbrerARUskYTz2JFFAEaxc/edit?usp=sharing">Merge Sort</a></>,
				<>- Start Faster Sorts</>,
				<>- Write pseudocode to HW Prioritization Problem</>,
				<><div className="cal-alert">Sort soft deadline</div></>,
				<><div className="cal-sp">AT takes CS content exam @ noon</div></>
			],
			[<>5 -- <span className="cal-sp">C DAY</span></>,
				<>CS 1 -- <a href="https://docs.google.com/presentation/d/15DUbH2rgTxgGX7GAk7DwUwa8DWcvshx4tKnombZEfAU/edit?usp=sharing">Turtle Graphics</a></>,
				<>- Jack to share past projects!</>,
				<>- Start <a href="https://github.com/noy-nac/write-your-name">Name</a></>,
				<>- Brain breaks</>,
				<><div className="cal-alert">SAT grade 11</div></>,
			],
			[<>6 -- Day A20</>,
				<>CS A -- <a href="https://docs.google.com/presentation/d/1ZM7dWK8n-aAiVTLTad45vf523Edtxiq4jkYRTOGG2Gg/edit?usp=sharing">Slides</a> + <span className="cal-tst">QUIZ</span></>,
				<>- Research project interest <a href ="https://docs.google.com/document/d/1ZDD7Q0lIAlrX1UwsPlK6gdQUJJDFqJBx-e7dYEPqgJQ/edit?usp=sharing">document</a></>,
				<><div className="cal-sp">Required career fair 10am-1pm</div></>,
				<><div className="cal-sp">Mr. Yin "subbing" for Mx. Mooney</div></>,
				<><div className="cal-alert">Deadline for all prior assignments</div></>,
			],
			[<>7 -- Day B19</>,
				<>CS 1 -- <a href="https://docs.google.com/presentation/d/1vgH2MHJkZ3kIgWufky1Ov0F8zR7VmTIWuUZDdezG8Mg/edit?usp=sharing">Turtle Graphics</a></>,
				<>- <a href="/class_code/snail.py" download>snail.py</a></>,
				<><div className="cal-alert">Deadline for all prior assignments</div></>,
			],
			[<>8 -- Day B20</>,
				<>CS 1 -- <a href="https://docs.google.com/presentation/d/1_-6MqVAf1_ySt4--U3kXIP8jMqhMluffOHkuRMWvBbw/edit?usp=sharing">Turtle Graphics</a></>,
				<><div className="cal-sp">End of grading cycle</div></>,
			],
		],
		[
			[<>Wk. 10</>],
			[<>Mar. 11</>, <><div className="cal-alert">SPRING BREAK</div></>],
			[<>12</>, <><div className="cal-alert">SPRING BREAK</div></>],
			[<>13</>, <><div className="cal-alert">SPRING BREAK</div></>],
			[<>14</>, <><div className="cal-alert">SPRING BREAK</div></>],
			[<>15</>, <><div className="cal-alert">SPRING BREAK</div></>],
		],
		
	]

	return (
		<>
			<table className="table table-bordered">
				<thead>
					<tr>
						{
							headers.map(colHead => <th className="mc-head" scope="col">{colHead}</th>)
						}
					</tr>
				</thead>
				{
					body.map(bodyRow =>

						<tbody>
							<tr>
								{
									bodyRow.map((rc, i, a) =>
									{									
										return (<td>
											{
												rc.map((value, index, array) => {
													return index > 0
														? <div className="mc-data">{value}</div>
														: <div className="mc-title">{ value}</div>
												})
											}
										</td>)
									})
								}
								
							</tr>
						</tbody>
					)
				}
			</table>
		</>
	);

}