

export default function CSch(props) {
	return (
		<>
			<h4 style={{ textAlign: "center" }}>
				C Day
			</h4>
			<table className="table table-bordered" style={{ textAlign: "center" }}>
				<thead>
					<tr style={{ fontWeight: 600 }}>
						<td>Period</td>
						<td>Time</td>
						<td>Content</td>
						<td>Date AT Assumes</td>
					</tr>
				</thead>
				<tbody>
					<tr className="table-warning">
						<td>1st</td>
						<td>9:05 - 9:50</td>
						<td>AP CS A</td>
						<td>1/29/2024</td>
					</tr>
					<tr>
						<td>2nd</td>
						<td>9:55 - 10:40</td>
						<td>AP CS Principles</td>
						<td>-</td>
					</tr>
					<tr>
						<td>3rd</td>
						<td>10:45 - 11:30</td>
						<td>Introduction to CS</td>
						<td>-</td>
					</tr>
					<tr>
						<td>4th</td>
						<td>11:35 - 12:25</td>
						<td>Conference</td>
						<td>-</td>
					</tr>
					<tr>
						<td>Lunch</td>
						<td>12:25 - 1:15</td>
						<td>-</td>
						<td>-</td>
					</tr>
					<tr>
						<td>5th</td>
						<td>1:20 - 2:05</td>
						<td>AP CS Principles</td>
						<td>-</td>
					</tr>
					<tr className="table-warning">
						<td>6th</td>
						<td>2:10 - 2:55</td>
						<td>Introduction to CS</td>
						<td>1/29/2014</td>
					</tr>
					<tr>
						<td>7th</td>
						<td>3:00 - 3:45</td>
						<td>Engineer Your World</td>
						<td>-</td>
					</tr>
					<tr>
						<td>8th</td>
						<td>3:50 - 4:35</td>
						<td>Conference</td>
						<td>-</td>
					</tr>
				</tbody>
			</table>
		</>
	);
}