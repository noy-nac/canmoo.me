
import Unipage from "../templates/Unipage";

export default function Algorithms() {

    return (<Unipage
        content={
            <>
                <h2>Algorithms</h2>
                <a href="https://www.youtube.com/watch?v=kM9ASKAni_s"></a>

                <iframe width="560" height="315" src="https://www.youtube.com/embed/kM9ASKAni_s?si=F-E9L6t90KID6LVZ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
            </>
        }
    />);

}