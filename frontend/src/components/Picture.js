

export default function Picture(props) {
    return (
        <div className="pb-0 pt-4">
            <img className="border rounded-pill border-5 border-whitesmoke mx-auto d-block" src={props.url} width={ props.size } />
        </div>
    );
}