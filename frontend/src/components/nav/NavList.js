
import { Link, useLocation } from 'react-router-dom'

import "./NavList.css"

export default function NavList(props) {

    let locationURL = useLocation().pathname;

    const links = props.nav_map.map(item =>
        <li class="my-nl nav-item">
            <Link
                className={item.url == locationURL
                    ? "my-nl nav-link active"
                    : "my-nl nav-link"
                }
                to={item.url}>
                {item.name}
            </Link>
        </li>
    );

    return (
        <div>
            {props.ordered
                ? < ol class="my-nl nav flex-column" >
                    {links}
                </ol>
                : <ul class="my-nl nav flex-column" >
                    {links}
                </ul>
            }
        </div>
    );
}
