
import { Link } from 'react-router-dom'

export default function BigName(props) {
    return (
        
        <h1 style={{ fontSize: props.size, textAlign: "center" }}>
            <Link to="/" style={{ color: 'inherit', textDecoration: 'inherit' }}>Canyon Mooney</Link>
        </h1>
    );
}
