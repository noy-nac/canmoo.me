
export default function Copyright() {
	return (
		<>
			<br/>
			<p>
				Copyright (c) 2024 by Canyon Mooney. Made with React, Bootstrap, and hosted on AWS.
			</p>
		</>
	);
}