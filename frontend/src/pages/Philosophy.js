
import Bipage from "../templates/Bipage";

export default function Philosophy() {

    return (
        <Bipage
            left={<></>}
            right={<>
                <h1>
                    My Teaching Philosphy
                </h1>
                <pre>By Canyon Mooney</pre>
                <p>
                    I believe we're all born being scientists.
                    Not that we all come out wearing a white lab coat, but rather, from day one, we all seek to make sense of the world in which we live.
                    To that end, the bedrock of my teaching philosophy is Piaget's theory of constructivism.
                    Students, you, and me all have an intrinsic desire to understand the world through our own experiences.
                    The beauty of learning is that it is the active process of this intimate, worldly sense-making.
                    And the construction of new knowledge knows no bounds.
                    Initially, we seek to fit the novel into that which we already know.
                    That is, we construct new knowledge using preexisting knowledge.
                    But upon the emergence of a real, experienced phenomenon that confuses our preexisting knowledge, our desire to understand only intensifies.
                    This disequilibrium is the driving force that compels us.
                    When we experience a mismatch between what we thought we knew and what the natural world shows us is true, we are compelled to update our conceptions about the world to accommodate the new aspect of reality.
                </p>
                <h2>Student Inquiry</h2>
                <p>
                    In practice, this means students learn best when they have experiences that are
                    meaningful in their personal journey to understand the world.
                    This is best done by capturing students' innate curiosity through a process called inquiry, in which students ask and answer their own questions that are relevant to them.
                    Then the role of the teacher is to support student inquiry with content connections, guiding questions, scaffolding, related activities, modeling, and
                    facilitating the direction of the class as a whole.
                    So that the results of the inquiry are made to crystalize, students share preconceptions before the lesson, then reflect on how their
                    understanding has changed afterwards.
                    I implement this with student-driven discussions built around airing preconceptions, finding the inconsistencies between preconceptions and new information, and articulating new hypotheses to explain the world.
                </p>
                <h2>Cooperative Learning</h2>
                <p>
                    Upon this aspect of constructivism lies the second tenet of my teaching philosophy, that learning is fundamentally a social-cultural activity.
                    As argued by Vygotsky, students are more capable with the help of others than alone.
                    By fostering a culture of care, support, and learning, the classroom becomes a community, students become accountable to their peers to support each other's learning, and the atmosphere becomes receptive to academic risk taking.
                    Every student pulls on every other student's zone of proximal development in a unique way.
                    Substantially different students will challenge each other in new and different ways.
                    In practice, this means the use of heterogeneous student grouping during lessons challenges students to continually contextualize and recontextualize their learning for others.
                    And students become more well-rounded and accepting of different perspectives as a result.
                </p>
                <h2>Student Agency</h2>
                <p>
                    The third tenet of my teaching philosophy is student agency.
                    In the classroom, agency means that students have a voice in topics such as setting class rules, the structure of assessments, and choosing lesson topics.
                    In this last point, the educator has a responsibility to help students engage with critical topics that affect them and their community.
                    By using what they learn in a way that is relevant to them, students come to believe in their abilities and build their self esteem.
                    Furthermore, by addressing critical topics in our society today, students become active participants in society.
                    And in practice, when students feel invested in the outcome of their work, they will put forth their best effort.
                    Consequently, the best way to assess what students know is to observe the artifacts students create as agents in society
                </p>
            </>}
        />
    )

}