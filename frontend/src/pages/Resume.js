/* I can moo and other reasons you should hire me */

import EmbededAccordion from "../components/EmbededAccordion";
import Picture from "../components/Picture";
import Bipage from "../templates/Bipage";



import Tabbed from "../templates/Tabbed";

import { ahs_contact_data } from "../data/Contact.js";


export default function Resume() {
	return (
		<Bipage
			left={
				<>
					<Picture url="/mooney-head.jpg" size="150em" />
					<h2 style={{ textAlign: "center" }}>Contact Info</h2>
					<EmbededAccordion data={ahs_contact_data} />
				</>
			}
			right={
				<>
					<h1>Resume</h1>

					<p>
						<a href="/mooney-resume.pdf" download>Resume (PDF)</a>
					</p>

					<h2>Education</h2>
					<h3><strong>Bachelor of Science (B.S.), Computer Science&mdash;Teaching</strong></h3>
					<h4>The University of Texas at Austin &bull; Expected Spring 2024 &bull; GPA: 3.7</h4>
					<p><ul>
						<li>Pursuing a Texas teaching certification in <strong>Computer Science</strong> as well as <strong>Mathematics</strong> (senior grades)</li>
						<li>Course work listing is available <a href="/education">here</a></li>
					</ul></p>

					<h2>Field Experience</h2>

					<h3><strong>Austin High School &bull; Austin, TX &bull; Spring 2024</strong></h3>
					<p><ul><li>Apprentice teaching</li></ul></p>
					
					<h3><strong>Akins High School &bull; Austin, TX &bull; Spring 2023</strong></h3>
					<p><ul><li>Taught a three day project-based lesson exploring object-oriented programming to Ms. Jessica Halley-Walker's class</li></ul></p>

					<h3><strong>Crockett High School &bull; Austin, TX &bull; Fall 2022</strong></h3>
					<p><ul><li>Taught a two-day lesson exploring digital forensics to Mr. Scott Cater's class</li></ul></p>

					<h3><strong>O. Henry Middle School &bull; Austin, TX &bull; Spring 2022</strong></h3>
					<p><ul><li>Taught two 5E lessons exploring cryptography to Mr. Luke Borders' middle school coding class</li></ul></p>

					<h3><strong>Reilly Elementary School &bull; Austin, TX &bull; Fall 2021</strong></h3>
					<p><ul><li>Taught two 5E lessons exploring weights and measurements to Ms. Jennan Sliman's elementary class</li></ul></p>

					<h2>Work Experience</h2>

					<h3><strong>Undergraduate Course Assistant &bull; Fall 2022, Fall 2023</strong></h3>
					<h4>UT Austin Dept. of Computer Science &bull; <a href="https://www.cs.utexas.edu/users/downing/cs373/">CS 373: Software Engineering</a> with <a href="https://www.cs.utexas.edu/users/downing/">Glenn Downing</a></h4>
					<p><ul>
						<li>Mentored approx. 60 students (30/sem.) over the course of a semester-long, full-stack web development project via weekly group meetings, help sessions, office hours, and ongoing advice and support</li>
						<li>Graded and gave feedback on project phase submissions</li>
						<li>Assumed a leadership role on the TA team by developing rubrics, coordinating grading standards, and organizing meetings</li>
						<li>Created instructional materials, including tutorials for <a href="https://gitlab.com/noy-nac/cs373-public-ta/-/tree/main/backend-ec2?ref_type=heads">AWS</a> and <a href="https://docs.google.com/presentation/d/1DPlhqTUEhG1yhtu0BN-JMLXR8ddFHWai5d8R4IYMifw/edit?usp=sharing">Postman</a></li>
					</ul></p>

					<h3><strong>Peer Mentor &bull; Spring 2023 &ndash; Fall 2023</strong></h3>
					<h4>UTeach Natural Sciences &bull; <a href="https://uteach.utexas.edu/step-1">UTS 101: Step 1</a> with <a href="https://uteach.utexas.edu/profiles/deanna-buckley">Deanna Buckley</a>, <a href="https://uteach.utexas.edu/profiles/alexandra-eusebi">Alex Eusebi</a>, and <a href="https://uteach.utexas.edu/profiles/shelly-rodriguez">Shelly Rodriguez</a> &bull; <a href="https://uteach.utexas.edu/step-2">UTS 110: Step 2</a> with <a href="https://uteach.utexas.edu/profiles/lynn-kirby">Lynn Kirby</a></h4>
					<p><ul>
						<li>Assisted with introductory teaching classes; held office hours; assisted with events to foster community; helped students write lesson plans; gave feedback on student practice teaches</li>
						<li>Wrote a program to generate discrepancy reports using teaching kit inventory data</li>
					</ul></p>

					<h3><strong>Electrical Engineering Intern &bull; June 2023 &ndash; July 2023</strong></h3>
					<h4>UTeach Outreach Summer STEM Camps &bull; <a href="https://outreach.uteach.utexas.edu/ut-prep-2">UT PREP 2</a> &bull; <a href="https://outreach.uteach.utexas.edu/life-science-summer-camps">Research in Life Sciences Camp</a></h4>
					<p><ul>
						<li>Taught the programming portion of Electrical Engineering II to four classes total over two 2.5 week sessions; documented curriculum in Canvas; worked with a teaching team to support students; accompanied students to other classes</li>
					</ul></p>

					<h3><strong>Software Development Intern &bull; June 2021 &ndash; August 2021</strong></h3>
					<h4>Teacher Retirement System of Texas &bull; HILOB Application Support</h4>
					<p><ul>
						<li>Participated on a scrum team to write and debug code for a full-stack application in Angular and Java; worked about 10 defects total during shadowing and on my own</li>
					</ul></p>

					<h3><strong>Undergraduate Teaching Assistant &bull; Spring 2021</strong></h3>
					<h4>UT Austin Dept. of Computer Science &bull; CS 103F: Ethical Fndns of Comp. Sci. with <a href="https://www.cs.utexas.edu/people/faculty-researchers/tina-peterson">Tina Peterson</a></h4>
					<p><ul>
						<li>Aided discussion among students; managed class assignments and organization via Canvas; graded short essay responses to case studies informing the process of ethical decision making</li>
					</ul></p>

					
				</>
			}
		/>
	);
}