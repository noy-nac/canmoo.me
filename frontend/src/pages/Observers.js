/* I can moo and other reasons you should hire me */

import EmbededAccordion from "../components/EmbededAccordion";
import Picture from "../components/Picture";
import Bipage from "../templates/Bipage";
import ABSch from "../cal/ABSch";
import PepSch from "../cal/PepSch";
import CSch from "../cal/CSch";



import Tabbed from "../templates/Tabbed";

import { ahs_contact_data } from "../data/Contact.js";


export default function Observers() {
	return (
		<Bipage
			left={
				<>
					<Picture url="/mooney-head.jpg" size="150em" />
					<h2 style={{ textAlign: "center" }}>Contact Info</h2>
					<EmbededAccordion data={ahs_contact_data} />
				</>
			}
			right={
				<>
					<h1>Observation Info</h1>

					<p>
						<strong>Austin High School, Room 301</strong><br/>
						1715 W. Cesar Chavez St. <br />
						Austin, TX 78703
					</p>

					<p>
						Main entrance is on the west side.
					</p>

					<p>
						Apprentice Teacher (AT): Canyon Mooney<br/>
						Cooperating Teacher (CT): David Yin
					</p>

					<p><a href="https://austin.austinschools.org/">Austin High Website</a></p>

					<h2>
						Schedule
					</h2>

					<p>
						In general, Mondays and Wednesdays are A days, while Tuesdays and Thursdays are B days. Fridays will vary.
						Please check the <a href="https://docs.google.com/document/d/1C4Bnlani7m_qKL_lgbCoCuYA_ltsOmqpkR0sianq75Y">A/B Calendar</a> to be certain.
					</p>

					<p> I usually arrive around 8:00-8:15 in the morning for tutorials.
						During this time, I'm also available for a pre-conference with observers of my AP CS A class (1st period).
					</p>


					<Tabbed
						tabs={[
							"A/B Days",
							"Pep Rally",
							"C Day",
						]}

						items={[
							<ABSch/>,
							<PepSch/>,
							<CSch/>,
						]}
					/>

					<h2>
						Lesson Plans
					</h2>
					<p>
						I will update this list weekly Saturday by 11:59pm.
					</p>
					<p>
						Apr. 22-26 (Wk. 16)
						<ul><li><a href="/lesson_plans/Mooney_LP_Wk_of_Apr_22.pdf" download>Maps + IRPs (AP CS A), Classes (Intro CS)</a></li></ul>
					</p>
					<p>
						Apr. 15-19 (Wk. 15)
						<ul><li><a href="/lesson_plans/Mooney_LP_Wk_of_Apr_15.pdf" download>Maps + IRPs (AP CS A), Classes (Intro CS)</a></li></ul>
					</p>
					<p>
						Apr. 8-12 (Wk. 14)
						<ul><li><a href="/lesson_plans/Mooney_LP_Wk_of_Apr_8.pdf" download>Linked Lists + IRPs (AP CS A), Classes (Intro CS)</a></li></ul>
					</p>
					<p>
						Apr. 1-5 (Wk. 13)
						<ul><li><a href="/lesson_plans/Mooney_LP_Wk_of_Apr_1.pdf" download>Interfaces + IRPs (AP CS A), Classes (Intro CS)</a></li></ul>
					</p>
					<p>
						Mar. 25-29 (Wk. 12)
						<ul><li><a href="/lesson_plans/Mooney_LP_Wk_of_Mar_25.pdf" download>Faster Sorts (AP CS A), Classes (Intro CS)</a></li></ul>
					</p>
					<p>
						Mar. 18-22 (Wk. 11)
						<ul><li><a href="/lesson_plans/Mooney_LP_Wk_of_Mar_18.pdf" download>Faster Sorts (AP CS A), Classes (Intro CS)</a></li></ul>
					</p>
					<p>
						Mar. 11-15 (Wk. 10)
						<ul><li>Spring break</li></ul>
					</p>
					<p>
						Mar. 4-8 (Wk. 9)
						<ul><li><a href="/lesson_plans/Mooney_LP_Wk_of_Mar_4.pdf" download>Faster Sorts (AP CS A), Turtle Graphics (Intro CS)</a></li></ul>
					</p>
					<p>
						Feb. 26-Mar. 1 (Wk. 8)
						<ul><li><a href="/lesson_plans/Mooney_LP_Wk_of_Feb_26.pdf" download>Faster Sorts (AP CS A), Turtle Graphics (Intro CS)</a></li></ul>
					</p>
					<p>
						Feb. 19-23 (Wk. 7)
						<ul><li><a href="/lesson_plans/Mooney_LP_Wk_of_Feb_19.pdf" download>Quadratic Sorts (AP CS A), 2D Lists (Intro CS)</a></li></ul>
					</p>
					<p>
						Feb. 12-16 (Wk. 6)
						<ul><li><a href="/lesson_plans/Mooney_LP_Wk_of_Feb_12.pdf" download>Search (AP CS A), 2D Lists (Intro CS)</a></li></ul>
					</p>
					<p>
						Feb. 5-9 (Wk. 5)
						<ul><li><a href="/lesson_plans/Mooney_LP_Wk_of_Feb_05.pdf" download>Abstract Classes (AP CS A), 2D Lists (Intro CS)</a></li></ul>
					</p>
					<p>
						Jan. 29-Feb. 2 (Wk. 4)
						<ul><li><a href="/lesson_plans/Mooney_LP_Wk_of_Jan_29.pdf" download>Abstract Classes (AP CS A), 2D Lists (Intro CS)</a></li></ul>
					</p>
					<p>
						Jan. 22-26 (Wk. 3)
						<ul>
							<li>Observing only</li>
							<li><a href="https://docs.google.com/presentation/d/1PAx9Djb16pJGP30FF9ClhUQDMTSGIYnbK80FCoQUakE/edit?usp=sharing">About Me Slides</a></li>
						</ul>
					</p>
					<p>
						Jan. 15-19 (Wk. 2)
						<ul><li>Not in field</li></ul>
					</p>
					<p>
						Jan. 8-12 (Wk. 1)
						<ul><li>Not in field</li></ul>
					</p>
				</>
			}
		/>
	);
}