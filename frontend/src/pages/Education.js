import Unipage from "../templates/Unipage";

export default function Education() {

    const cs_classes = [
        {
            sub: "Applied Topics", classes: [
                "CS 354 - Computer Graphics",
                "CS 378 - Ethical Hacking",
                "CS 373 - Software Engineering",
                "CS 342 - Neural Networks",
                "CS 104C - Competitive Programming",
            ]
        },
        {
            sub: "Theory Topics", classes: [
                "CS 375 - Compilers",
                "CS 346 - Cryptography",
                "CS 343 - Artificial Intelligence",
            ]
        },
        {
            sub: "Critical Topics", classes: [
                "CS 349 - Contemporary Issues in Computer Science",
                "CS 103F - Ethical Foundations of Computer Science",
                "CS 302 - Computer Fluency"
            ]
        },
        {
            sub: "Foundational Topics", classes: [
                "CS 331 - Algorithms and Complexity",
                "CS 439 - Principles of Computer Systems",
                "CS 429 - Computer Organization and Architecture",
                "CS 314 - Data Structures",
                "CS 312 - Introduction to Programming"
            ]
        },
    ];

    const math_classes = [
        {
            sub: "Advanced", classes: [
                "M 373K - Algebraic Structures",
                "M 328K - Introduction to Number Theory",
                "M 333L - Structure of Modern Geometry"
            ]
        },
        {
            sub: "Intermediate", classes: [
                "M 340L - Matricies and Matrix Calculations",
                "CS 311 - Discrete Math",
                "M 362K - Probability I",
                "SDS 302 - Data Analysis"
            ]
        },
        {
            sub: "Foundational", classes: [
                "M 408D - Sequences, Series, and Multivariable Calculus",
                "M 408C - Differential and Integral Calculus",
                "M 315C - Foundations, Functions, and Regression Models",
            ]
        }
    ];

    const edu_classes = [
        {
            sub: "Field Experience", classes: [
                "EDC 365E - Project-Based Instruction (Highschool grades)",
                "EDC 365D - Classroom Interactions (Highschool grades)",
                "UTS 110 - Step 2 (Middle grades)",
                "UTS 101 - Step 1 (Elementary grades)",
            ]
        },
        {
            sub: "Theory & Pedagogy", classes: [
                "PHY 341 - Research Methods (Scientific Inquiry)",
                "HIS 329U - Perspectives on Science & Math",
                "EDC 365C - Knowing and Learning in Math & Science",
            ]
        }
       
    ];

    const other_classes = [
        {
            sub: "Languages", classes: [
                "JPN 601D - Japanese I",
                "LAT 507 - First-Year Latin II",
                "LAT 506 - First-Year Latin I",
            ],
        },
        {
            sub: "Natural Sciences", classes: [
                "NEU 330 - Neural Systems I",
                "PHY 303L - Engineering Physics II (Electromagnetism)",
                "PHY 303K - Engineering Physics I (Mechanics)",
                "PHY 103N - Lab for PHY 303L",
                "PHY 103M - Lab for PHY 303K",
                "CH 301 - Principles of Chemistry I",
            ]
        },
        {
            sub: "Liberal Arts", classes: [
                "PHL 310 - Knowledge and Reality",
                "PHL 303M - Mind and Body",
                "PSY 306 - Introduction to Human Sexuality",
                "HIS 315K - The United States, 1492-1865",
                "HIS 315K - The United States Since 1865",
                "GOV 310L - American Government",
                "E 316L - British Literature",
                "RHE 306 - Rhetoric and Writing",
            ]
        },
        {
            sub: "Miscellanous", classes: [
                "ACC 310F - Foundations of Accounting",
                "AET 304 - Foundations of Art & Entertainment Technology",
                "UGS 303 - How Things Work",
                "ECO 304L - Introduction to Macroeconomics",
            ]
        },

    ];

    const course_areas = [
        { name: "Computer Science", data: cs_classes },
        { name: "Mathematics", data: math_classes },
        { name: "Education", data: edu_classes },
        { name: "Other", data: other_classes }
    ];

    const course_list =
        course_areas.map((area) =>
            <div>
                <h4>{area.name}</h4>
                {
                    area.data.map((item) => 
                        <div>
                            <h5>{item.sub}</h5>
                            <ul>
                                {
                                    item.classes.map(course => <li>{course}</li>)
                                }
                            </ul>
                        </div>
                            
                    )
                }
            </div>
        );

    return (<Unipage
        content={
            <>
                <h2>Education</h2>
                <p>
                    Bachelor of Science (B.S.), Computer Science&mdash;Teaching
                </p>
                <p>
                    The University of Texas at Austin
                </p>

                <h3>Course Work</h3>
                {course_list}
            </>
        }
    />);
}