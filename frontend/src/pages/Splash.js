/* I can moo and other reasons you should hire me */

import EmbededAccordion from "../components/EmbededAccordion";
import Picture from "../components/Picture";
import Bipage from "../templates/Bipage";
import LCal24SpH1 from "../cal/LCal24SpH1";
import LCal24SpH2 from "../cal/LCal24SpH2";

import Tabbed from "../templates/Tabbed";

import { ahs_contact_data } from "../data/Contact.js";


export default function Splash() {
	return (
		<Bipage
			left={
				<>
					<Picture url="./mooney-head.jpg" size="150em" />

					{/*<EmbededAccordion data={contact_data}/>*/}

					<h2 style={{ textAlign: "center" }}>Contact Info</h2>

					<EmbededAccordion data={ahs_contact_data}/>


				</>
			}
			right={
				<>
					<h1>
						Mx. Mooney's Computer Science Class!
					</h1>

					<p>
						Hi, my name is Canyon Mooney.
						I'm a student in the UTeach Natural Sciences program pursuing my teaching certification in senior grades computer science.
						This semester, I am apprentice teaching under David Yin at Austin High School. 
					</p>

					<p>
						I anticipate graduating with my B.S. in Computer Science from UT Austin this Spring.
						I've also completed substantial coursework in mathematics and teaching methods.
						Upon graduation, my goal is to teach secondary computer science and mathematics in central Texas.
					</p>

					<p>
						If you're here for information on observing me, please use the "For Observers" tab up top!
					</p>

					<p>
						<strong></strong>
					</p>

					<h2>
						Spring 2024 Course Calendar
					</h2>

					<p>
						<strong>Please do NOT print this calendar, as it WILL change!</strong>
					</p>

					<Tabbed
						tabs={[
							"3rd 9 Weeks",
							"4th 9 Weeks",
						]}

						items={[
							<LCal24SpH1 />,
							<LCal24SpH2 />,
						]}
					/>
				</>
			}
		/>
	);
}