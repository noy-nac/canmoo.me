import { useParams } from "react-router-dom";
import NavList from "../components/nav/NavList";
import Picture from "../components/Picture";
import Bipage from "../templates/Bipage";

import portfolioData from '../data/portfolioLite.json';
import Tabbed from "../templates/Tabbed";


//const loadData = () => JSON.parse(JSON.stringify(jsonData));

const teaching_nav = [
    { name: "Profile Information", url: "/portfolio/uteach/a"},
    { name: "Equity and Inclusive Design", url: "/portfolio/uteach/b"},
    { name: "Classroom Environment and Managment", url: "/portfolio/uteach/c" },
    { name: "Lesson Design", url: "/portfolio/uteach/d" },
    { name: "Lesson Implementaion", url: "/portfolio/uteach/e" },
    { name: "Content Knowledge", url: "/portfolio/uteach/f" },
];

export default function Teaching() {

    const { section = "a" } = useParams();

    const tabNames = [];  

    portfolioData.map(item =>
        item.major.toLowerCase() == section.toLowerCase() && item.minor != "0"
            ? tabNames.push(item.name)
            : null);

    const tabItems = [];

    portfolioData.map((item) =>
        item.major.toLowerCase() == section.toLowerCase() && item.minor != "0"
            ?
            tabItems.push(<>

                <h3>{item.name}</h3>
                {item.prompt.map(para => <p><strong>{para}</strong></p>)}
                {item.prose.map(para => <p>{para}</p>)}
                <h4>
                    Artifacts
                </h4>
                <p>
                    <ul>
                        {item.artifacts.map(file =>
                            <li><a href={"/prelim_portfolio_v1/" + `${file}`}>{file}</a></li>)
                        }
                    </ul>
                </p>
            </>)
            : null
    );

    return (
        <Bipage
            left={<>
                <Picture url="/mooney-head.jpg" size="150em" />
                <h2 style={{ textAlign: "center" }}>Proficiencies</h2>
                <NavList
                    nav_map={teaching_nav}
                    ordered={false}
                />
                </>}
            right={<>
                <h1>UTeach Preliminary Portfolio</h1>
                <p>
                    Canyon Mooney, Computer Science (Senior Grades)<br />
                    UTeach Natural Sciences<br/>
                    The University of Texas at Austin
                </p>

                <p>
                    Please navigate using the red sidebar by clicking to proficiency you wish to view.
                </p>

                {
                    portfolioData.map((item) => 
                        item.major.toLowerCase() == section.toLowerCase()
                            ? item.minor == "0"
                                ? <>
                                    <h2>{item.name}</h2>
                                    {item.prompt.map(para => <p><strong>{para}</strong></p>)}
                                  </>
                                : null
                            : null
                    )
                }
                
                <Tabbed
                    tabs={tabNames}
                    items={tabItems}
                />
            </>}
        />
    );
}